# 发布资源

```injectablephp
php artisan vendor:publish --provider="GuanChanghu\Library\Providers\AppServiceProvider"
```

# 修改配置文件
## filesystems.php

```injectablephp
'disks' => [
    'public' => [
        'driver' => 'local',
        'root' => storage_path('app/public'),
        'url' => env('APP_URL') . '/storage',
        'visibility' => 'public',
    ],
    'client' => [
        'driver' => 'local',
        'root' => app_path('Http/Clients'),
    ],
    'root' => [
        'driver' => 'local',
        'root' => base_path(),
    ],
    'file' => [
        'driver' => 'local',
        'root' => storage_path('file'),
    ],
    'storage' => [
        'driver' => 'local',
        'root' => storage_path(),
    ],
    'resource' => [
        'driver' => 'local',
        'root' => resource_path(),
    ],
    'lang' => [
        'driver' => 'local',
        'root' => resource_path('lang'),
    ],
]
```

## logging.php(根据客户端自行配置)

```injectablephp
'channels' => [
    'single' => [
        'driver' => 'single',
        'path' => storage_path('logs/laravel/laravel.log'),
        'level' => env('LOG_LEVEL', 'debug'),
    ],

    'daily' => [
        'driver' => 'daily',
        'path' => storage_path('logs/laravel/laravel.log'),
        'level' => 'debug',
        'days' => 720,
        'handler' => Monolog\Handler\BrowserConsoleHandler::class,
        'formatter' => Monolog\Formatter\LineFormatter::class,
        'formatter_with' => [
            'format' => "[%datetime%] [" . env('SERVICE_TAG', 'api') . "] %channel%.%level_name%: %message% %context%\n",
            'dateFormat' => 'Y-m-d H:i:s.u',
        ],
    ],
    'client_admin' => [
        'driver' => 'daily',
        'path' => storage_path('logs/client/admin/laravel.log'),
        'level' => 'debug',
        'days' => 720,
        'handler' => Monolog\Handler\BrowserConsoleHandler::class,
        'formatter' => Monolog\Formatter\LineFormatter::class,
        'formatter_with' => [
            'format' => "[%datetime%] [" . env('SERVICE_TAG', 'api') . "] %channel%.%level_name%: %message% %context%\n",
            'dateFormat' => 'Y-m-d H:i:s.u',
        ],
    ],
    'slow_admin' => [
            'driver' => 'daily',
            'path' => storage_path('logs/slow/admin/laravel.log'),
            'level' => 'debug',
            'days' => 720,
            'handler' => Monolog\Handler\BrowserConsoleHandler::class,
            'formatter' => Monolog\Formatter\LineFormatter::class,
            'formatter_with' => [
                'format' => "[%datetime%] [" . env('SERVICE_TAG', 'api') . "] %channel%.%level_name%: %message% %context%\n",
                'dateFormat' => 'Y-m-d H:i:s.u',
            ],
        ],
]
```
# 任务调度

## 登录授权过期

```injectablephp
$schedule->command('auth:expire')
    ->hourly()
    ->withoutOverlapping()
    ->runInBackground()
    ->appendOutputTo(storage_path('logs/schedule/auth/expire.' . TimeUtil::getFormatDateYm() . '.log'));
```

## 邮箱发送统计任务

```injectablephp
$schedule->command('email:send:statistics')
    ->daily()
    ->withoutOverlapping()
    ->runInBackground()
    ->appendOutputTo(storage_path('logs/schedule/email/send.statistics.' . TimeUtil::getFormatDateY() . '.log'));
```

## 其他命令

```injectablephp
// apiDoc生成
php artisan generate:apiDoc {client=web} {version=1}

// 简单数学题生成
php artisan generate:simple:math

// 获得mac地址
php artisan mac:address

// 更新权限
php artisan permission:update

// 项目安装
php artisan project:install

// 行政地域导入
php artisan lead:territory

// 导入答题
php artisan lead:question

// 翻译
php artisan translate {form=zh} {to=en} {file?}

// 钱包初始化
php artisan wallet:initialize
```

## env更改

```injectablephp
// env

// thrift
THRIFT_SERVICE_ENABLED=false
THRIFT_CODE=""

// 翻译
TRANSLATE_DEFAULT_DRIVER=baidu
TRANSLATE_DEFAULT_SPARE_DRIVER=baidu
TRANSLATE_DEFAULT_FROM=en
TRANSLATE_DEFAULT_TO=zh
TRANSLATE_BAIDU_BASE_URL="https://api.fanyi.baidu.com/api/trans/vip/translate"
TRANSLATE_BAIDU_APP_ID=
TRANSLATE_BAIDU_APP_KEY=
TRANSLATE_BAIDU_INTERVAL=0
TRANSLATE_YOU_DAO_BASE_URL="https://openapi.youdao.com/api"
TRANSLATE_YOU_DAO_APP_ID=
TRANSLATE_YOU_DAO_APP_KEY=
TRANSLATE_YOU_DAO_INTERVAL=0
TRANSLATE_GOOGLE_BASE_URL="http://translate.google.cn/translate_a/single"
TRANSLATE_GOOGLE_APP_ID=
TRANSLATE_GOOGLE_APP_KEY=
TRANSLATE_GOOGLE_INTERVAL=0
```



