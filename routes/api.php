<?php

use GuanChanghu\Library\Http\Controllers\ClientController;
use GuanChanghu\Library\Http\Controllers\UploadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


$allClientStr = implode('|', config('guan-changhu.clients.list'));

Route::post('/{client}', [ClientController::class, 'index'])->where('client', $allClientStr)->name('client');

Route::post('/upload/{client}', [UploadController::class, 'index'])->where('client', $allClientStr)->name('upload');

