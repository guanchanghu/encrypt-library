<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWalletLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_wallet_logs', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('用户ID');

            $table->unsignedTinyInteger('wallet')->default(0)->comment('钱包');

            $table->unsignedTinyInteger('finance_type')->default(0)->comment('财务类型');

            $table->unsignedTinyInteger('symbol')->default(0)->comment('符号；0-收入；1-支出');

            $table->decimal('before_money', 12, 2)->default(0)->comment('钱包变化前的金额');
            $table->decimal('money', 12, 2)->default(0)->comment('钱包变化的金额');
            $table->decimal('after_money', 12, 2)->default(0)->comment('钱包变化后的金额');

            $table->string('remark')->default('')->comment('备注');

            $table->index(['user_id', 'wallet', 'finance_type', 'symbol'], 'uwl_ui_w_ft_s');

            $table->index(['wallet', 'finance_type', 'symbol'], 'uwl_w_ft_s');

            $table->index(['finance_type', 'symbol'], 'uwl_ft_s');

            $table->timestamps();

            $table->comment('用户钱包日志表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_wallet_logs');
    }
}
