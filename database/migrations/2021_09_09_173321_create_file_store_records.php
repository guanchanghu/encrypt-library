<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateFileStoreRecords
 */
class CreateFileStoreRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_store_records', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('用户ID');

            $table->string('client', 25)->default('')->comment('客户端');

            $table->string('module')->default('')->comment('所属模块');

            $table->string('type', 25)->default('image')->comment('文件类型');
            $table->string('extension', 75)->default('jpg')->comment('文件扩展名');

            $table->string('relative')->default('')->comment('文件相对路径');
            $table->string('path')->default('')->comment('文件全路径');

            $table->string('original_file_name')->default('')->comment('原先文件名称');
            $table->unsignedBigInteger('size')->default(0)->comment('文件大小');


            $table->unsignedTinyInteger('status')->default(1)->comment('状态;0-不显示;1-显示');

            $table->timestamps();


            $table->index(['user_id', 'client', 'module', 'type', 'status']);

            $table->index(['client', 'module', 'type', 'status']);

            $table->index(['type', 'status'], 'fsr_ts_index');

            $table->index(['type', 'extension', 'original_file_name', 'size'], 'fsr_teos_index');

            $table->comment('文件存储记录');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_store_records');
    }
}
