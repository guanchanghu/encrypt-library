<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_accounts', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('用户ID');

            $table->unsignedTinyInteger('type')->default(0)->comment('类型;0-银行卡;1-支付宝;2-微信;3-账户微信');

            $table->string('real_name', 25)->default('')->comment('真实姓名');

            $table->string('account', 25)->default('')->comment('账户号;支付宝就是账户号;微信是openid/微信账号;银行卡就是卡号');

            $table->string('qr_code')->default('')->comment('收款码');

            $table->string('mobile', 14)->default('')->comment('预留手机号');

            $table->string('bank_subbranch', 55)->default('')->comment('开户行支行');

            $table->timestamps();

            $table->index(['user_id', 'type']);

            $table->comment('用户账户表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_accounts');
    }
}
