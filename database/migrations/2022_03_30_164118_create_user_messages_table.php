<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_messages', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('接受者用户ID;如果是0表示发送给全部');

            $table->unsignedBigInteger('message_content_id')->default(0)->comment('消息表ID');

            $table->unsignedInteger('read_num')->default(0)->comment('阅读数量');

            $table->unsignedBigInteger('jump_link_count')->default(0)->comment('跳转链接次数');

            $table->timestamps();

            $table->unique(['user_id', 'message_content_id']);
            $table->index(['message_content_id']);

            $table->comment('用户消息表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_messages');
    }
}
