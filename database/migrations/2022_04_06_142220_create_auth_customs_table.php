<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateAuthCustomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_customs', function (Blueprint $table) {

            $table->string('id', 39)->default('')->comment('主键');

            $table->unsignedBigInteger('user_id')->default(0)->comment('用户ID');

            $table->string('client', 25)->default('')->comment('客户端');

            $table->timestamp('expires_at')->nullable()->comment('过期时间');

            $table->timestamps();

            $table->primary(['id']);
            $table->unique(['user_id', 'client']);

            $table->comment('自定义授权表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_customs');
    }
}
