<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_wallets', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('用户ID');

            $table->unsignedTinyInteger('wallet')->default(0)->comment('钱包');

            $table->decimal('money', 12, 2)->default(0)->comment('金额');
            $table->decimal('grand_total', 12, 2)->default(0)->comment('累计金额；如果是增加金额就增加上');

            $table->unique(['user_id', 'wallet']);

            $table->timestamps();

            $table->comment('用户钱包表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_wallets');
    }
}
