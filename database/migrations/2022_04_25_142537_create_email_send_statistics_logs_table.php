<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailSendStatisticsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_send_statistics_logs', function (Blueprint $table) {
            $table->id();

            $table->string('email')->default('')->comment('发送邮件的邮箱');

            $table->date('date')->nullable()->comment('统计日期');

            $table->unsignedInteger('num')->default(0)->comment('发送的数量');

            $table->timestamps();

            $table->unique(['email', 'date',]);

            $table->comment('邮件发送统计表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_send_statistics_logs');
    }
}
