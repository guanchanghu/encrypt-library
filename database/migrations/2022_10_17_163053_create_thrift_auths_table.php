<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThriftAuthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thrift_auths', function (Blueprint $table) {
            $table->id();

            $table->char('code', 39)->default('')->comment('code码,不唯一');

            $table->timestamps();

            $table->unique(['code',]);

            $table->comment('thrift认证表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thrift_auths');
    }
}
