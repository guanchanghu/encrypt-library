<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_logs', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('支付人用户ID');

            $table->char('order_no', 39)->default('')->comment('订单号');

            $table->decimal('amount', 12, 2)->default(0)->comment('订单金额,单位元');

            $table->string('pay_driver', 32)->default('')->comment('支付驱动;微信/支付宝/银行卡');

            $table->string('pay_way', 32)->default('')->comment('支付方式;如app,mini');

            $table->string('remark')->default('')->comment('备注');

            $table->unsignedTinyInteger('status')->default(0)->comment('状态;0-未成功;1-已成功');
            $table->timestamp('paid_at')->nullable()->comment('付款时间');

            $table->timestamps();

            $table->unique(['order_no']);

            $table->index(['pay_driver', 'pay_way', 'status']);

            $table->comment('支付日志表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_logs');
    }
}
