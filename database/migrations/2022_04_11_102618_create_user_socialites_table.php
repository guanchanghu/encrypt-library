<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSocialitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_socialites', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('用户ID');

            $table->string('provider')->default('')->comment('供应商，如：wechat');
            $table->string('provider_id')->default('')->comment('供应商ID，如：unionid');
            $table->string('provider_second_id')->default('')->comment('供应商二级ID，如：openid');

            $table->string('nickname', 55)->default('')->comment('昵称');

            $table->unsignedTinyInteger('gender')->default(2)->comment('性别;0-女;1-男;2-未知');

            $table->string('province', 25)->default('')->comment('省份');
            $table->string('city', 25)->default('')->comment('城市');
            $table->string('country', 25)->default('')->comment('国家');

            $table->string('email', 55)->default('')->comment('email');

            $table->string('avatar')->default('')->comment('头像');

            $table->string('remember_token', 100)->default('')->comment('token');

            $table->timestamps();

            $table->unique(['user_id', 'provider', 'provider_id']);

            $table->unique(['provider', 'provider_id']);
            $table->unique(['provider', 'nickname']);
            $table->unique(['provider', 'email']);

            $table->comment('用户第三方表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_socialites');
    }
}
