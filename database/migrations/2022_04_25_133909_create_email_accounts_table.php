<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_accounts', function (Blueprint $table) {
            $table->id();

            $table->string('host')->default('')->comment('地址');
            $table->string('port')->default('')->comment('端口');

            $table->string('username')->default('')->comment('用户');
            $table->string('password')->default('')->comment('密码');
            $table->string('encryption')->default('')->comment('加密方式');

            $table->unsignedTinyInteger('is_enabled')->default(1)->comment('是否开启;0-不开启;1-开启');

            $table->unsignedInteger('today_num')->default(0)->comment('今天发送数量');
            $table->unsignedInteger('daily_limit')->default(0)->comment('每日上限');

            $table->timestamps();

            $table->index(['is_enabled', 'today_num']);

            $table->unique('username');

            $table->comment('邮箱账户表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_accounts');
    }
}
