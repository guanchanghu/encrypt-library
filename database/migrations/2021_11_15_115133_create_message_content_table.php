<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_content', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->default(0)->comment('发送者用户ID');

            $table->unsignedTinyInteger('type')->default(0)->comment('类型;0-单发;1-指定人(多个用户);2-全部用户;');

            $table->unsignedTinyInteger('clients')->default(2)->comment('客户端;1-admin用户;2-web用户;4-h5用户;8-wap用户;16-agent用户;多个客户端就是他们的|');

            $table->string('title')->default('')->comment('标题');

            $table->string('icon')->default('')->comment('icon');

            $table->string('link')->default('')->comment('跳转链接');

            $table->unsignedBigInteger('send_user_num')->default(1)->comment('发送的用户数量');

            $table->unsignedInteger('read_user_num')->default(0)->comment('阅读的用户数量');
            $table->unsignedInteger('read_num')->default(0)->comment('阅读数量');

            $table->unsignedBigInteger('jump_link_count')->default(0)->comment('跳转链接次数');
            $table->unsignedBigInteger('jump_link_user_num')->default(0)->comment('跳转链接用户数量');

            $table->string('abstract')->default('')->comment('摘要');

            $table->text('content')->nullable()->comment('消息内容');

            $table->timestamps();

            $table->index(['user_id', 'type']);
            $table->index(['title']);

            $table->comment('消息TXT表');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_content');
    }
}
