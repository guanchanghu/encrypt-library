<?php
/**
 * 邮件支持加密
 * Created on 2022/4/26 10:24
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * @author 管昌虎
 * Class BaseMail
 * @tag encryption free
 * @package GuanChanghu\Library\Mail
 * Created on 2022/4/26 10:30
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
abstract class BaseMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    protected string $key = '';

    /**
     * @return string
     */
    public function address(): string
    {
        if(!$this->to){
            return '';
        }

        return current($this->to)['address'];
    }
}
