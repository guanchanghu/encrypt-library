<?php
/**
 * 存储消息
 * Created on 2022/9/16 10:25
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Listeners;

use GuanChanghu\Library\Facades\Log;
use Throwable;
use Exception;
use GuanChanghu\Library\Events\WebSocketSending;
use GuanChanghu\Traits\SafeOperation\Facade as SafeOperation;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * @author 管昌虎
 * Class StorageMessage
 * @tag encryption free
 * @package GuanChanghu\Library\Listeners
 * Created on 2022/9/16 10:25
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class StorageMessage implements ShouldQueue
{
    use SafeOperation;

    /**
     * 任务将被发送到的队列的名称
     *
     * @var string
     */
//    public string $queue = 'websocket';

    /**
     * 任务可尝试的次数
     *
     * @var int
     */
    public int $tries = 3;

    /**
     * 在超时之前任务可以运行的秒数
     *
     * @var int
     */
    public int $timeout = 120;

    /**
     * 延迟执行时间
     * @var int
     */
    public int $delay = 0;

    /**
     * Handle the event.
     *
     * @param WebSocketSending $event
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function handle(WebSocketSending $event)
    {

    }

    /**
     * @param WebSocketSending $event
     * @param Throwable $throwable
     * @return void
     */
    public function failed(WebSocketSending $event, Throwable $throwable)
    {
        Log::setClient($event->client)->error('uuid：' . $event->uuid, [__METHOD__, $event, $throwable->getFile(). ':' . $throwable->getLine(), $throwable->getMessage()]);
    }
}
