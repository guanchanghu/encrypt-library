<?php
/**
 * 自定义程序基础门面
 * Created on 2022/8/26 14:26
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Processes\Base;

use Hhxsv5\LaravelS\Swoole\Process\CustomProcessInterface;
use Swoole\Http\Server;
use Swoole\Process;

/**
 * @author 管昌虎
 * Class Facade
 * @method static string getProcessKey(Server $swoole, Process $process)
 * @method static int getProcessTotal(Server $swoole)
 * @method static string getProcessNum(Server $swoole, Process $process)
 * @package GuanChanghu\Library\Processes\Base
 * Created on 2022/8/26 14:26
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
abstract class Facade extends Encrypt implements CustomProcessInterface
{
    /**
     * The run callback of process
     * @param Server $swoole
     * @param Process $process
     * @return void
     */
    abstract public static function callback(Server $swoole, Process $process);

    /**
     * Trigger this method on receiving the signal SIGUSR1
     * @param Server $swoole
     * @param Process $process
     * @return void
     */
    abstract public static function onReload(Server $swoole, Process $process);
}
