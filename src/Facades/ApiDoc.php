<?php


namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Client\Module;
use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\ApiDoc as ApiDocContract;

/**
 * Class RSA
 * @method static array parserClass(Module $class)
 * @method static array parserMethod(Module $class, string $method)
 * @method static bool generateApiDoc(string $client = '', int $version = 0)
 * @method static string parse(string $doc = '')
 * @package GuanChanghu\Library\Facades
 */
class ApiDoc extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ApiDocContract::class;
    }
}
