<?php
/**
 * 实名认证门面
 * Created on 2022/5/10 16:26
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Authentication\Factory;
use GuanChanghu\Library\Contracts\Authentication\Core;
use GuanChanghu\Library\Models\AuthenticationLog;
use GuanChanghu\Library\Models\User;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Authentication
 * @method static Core driver(string $driver = '')
 * @method static array token(User $user, string $realName, string $idNumber, string $frontImage = '', string $backImage = '', string $handImage = '', array $params = [])
 * @method static AuthenticationLog verify(User $user, string $realName = '', string $idNumber = '', string $orderNo = '', string $frontImage = '', string $backImage = '', string $handImage = '', array $params = [])
 * @package GuanChanghu\Library\Facades
 * Created on 2022/5/10 16:26
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Authentication extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
