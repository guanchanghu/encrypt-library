<?php
/**
 * 支付
 * Created on 2022/3/17 16:54
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Pay\Core;
use GuanChanghu\Library\Contracts\Pay\Factory;
use GuanChanghu\Library\Models\User;
use GuanChanghu\Library\Models\UserAccount;
use Illuminate\Support\Facades\Facade;
use Psr\Http\Message\ResponseInterface;
use Yansongda\Supports\Collection;

/**
 * @author 管昌虎
 * Class Pay
 * @method static Core driver(string $driver)
 * @method static ResponseInterface|Collection pay(string $way, string $outTradeNo, float $amount, string $description, User|null $user = null, string $attach = '', array $other = array())   单位元
 * @method static ResponseInterface callback(array $callback, array $param)
 * @method static Collection find(string|array $order)
 * @method static array|Collection refund(array $order)
 * @method static array|Collection close(string|array $order)
 * @method static array|Collection cancel(string|array $order)
 * @method static bool transfer(UserAccount $userAccount, string $outTradeNo, float $amount, string $title, string $remark)
 * @method static bool isConfig()
 * @package GuanChanghu\Library\Facades
 * Created on 2022/3/17 16:55
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @document https://pay.yansongda.cn    参数参展此链接 版本 3
 */
class Pay extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
