<?php
/**
 * 第三方授权
 * Created on 2022/4/12 11:53
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Socialite as SocialiteContract;
use GuanChanghu\Library\Models\User;
use GuanChanghu\Library\Models\UserSocialite;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Socialite
 * @method static UserSocialite weChat(string $code, User|null $user = null, array $clients = [])                       用户和客户端必须传递一个
 * @package GuanChanghu\Library\Facades
 * Created on 2022/4/12 11:53
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Socialite extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return SocialiteContract::class;
    }

}
