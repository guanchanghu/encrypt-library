<?php


namespace GuanChanghu\Library\Facades;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\Territory\Factory;
use GuanChanghu\Library\Contracts\Territory\Core;

/**
 * Class Territory
 * @method static Core store(string $store = '')
 * @method static Collection|array select($collection, array $fields = [])
 * @method static array getTerritoryById(int $id)
 * @method static array getTerritoryByShortName(string $shortName)
 * @method static array getTerritoryByName(string $name)
 * @method static array getTerritoryByUniteName(string $uniteName)
 * @method static string getPrimaryKey()
 * @method static Collection getTreeTerritory()
 * @method static Collection getTerritorySubordinateByParentId(int $parentId)
 * @method static Collection getTreeTerritoryById(int $id)
 * @method static Collection getTreeTerritoryByShortName(string $shortName)
 * @method static Collection getTreeTerritoryByName(string $name)
 * @method static Collection getTreeTerritoryByUniteName(string $uniteName)
 * @method static bool clear()
 * @package GuanChanghu\Library\Facades
 */
class Territory extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
