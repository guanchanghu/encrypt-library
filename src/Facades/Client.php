<?php


namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Client\Core;
use GuanChanghu\Library\Contracts\Client\Factory;
use GuanChanghu\Library\Contracts\Client\Module as ModuleContract;
use Illuminate\Routing\Controller;
use GuanChanghu\Library\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Client
 * @method static Core client(string $client)                                                                           获得指定客户端下面的核心实例
 * @method static Core setController(Controller $controller)
 * @method static Core setRequestData(array $requestData)
 * @method static Core setUser(User $user)                                                                              设置用户
 * @method static array run()                                                                                           运行模块
 * @method static array|string|Collection|Model single(string $module, int $interface, int $userId = 0, int $version = 1, array $list = [], array $form = [])              模拟
 * @method static int checkInterfaceCodeModule(int $version, string $module, int $interface)                            获得版本号,指定模块,接口,向下查找指定版本号
 * @method static ModuleContract getModuleInstance(int $version, string $module)                                        获得当前客户端,指定版本模块实例
 * @method static Collection getAllModules(int $version = 0)                                                            获得当前客户端当前版本的全部模块,下面还有分组,版本下面才有模块
 * @method static int getMaxVersion()                                                                                   获得当前客户端最大版本
 * @method static string getClient()                                                                                    获得客户端名字
 * @method static string getUuid()                                                                                      获得当前客户端当前接口的uuid
 * @method static throttle(int $timestamp, int $random, User|null $user = null)                                         判断是否是重复访问,单位秒
 * @method static checkMillisecondTimestamp(int $millisecond)                                                                      检查时间戳是否合法,单位毫秒
 * @method static array getAllClient()                                                                                  获得全部客户端
 * @method static array getTreeClient()                                                                                 获得当前客户端下面模块tree
 * @method static array moduleSwitch()                                                                                  模块开关
 * @method static bool clientPermissionSwitch()                                                                         客户端权限开关
 * @method static bool clearAllCache()                                                                                  清除缓存
 * @method static array getClientDefault()                                                                              获得用户默认客户端列表
 * @method static array getDefaultForbidModule()                                                                        获得客户端默认不分配的模块
 * @method static array getDefaultForbidPermission()                                                                    获得客户端默认不分配的权限
 * @method static int getExpire(string $client)
 * @method static bool isClient(string $client)
 * @package GuanChanghu\Library\Facades
 * Created on 2022/8/11 18:12
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Client extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
