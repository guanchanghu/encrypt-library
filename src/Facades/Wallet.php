<?php


namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Wallet\Core;
use GuanChanghu\Library\Contracts\Wallet\Factory;
use GuanChanghu\Library\Models\UserWallet;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * Class Models
 * @method static Core driver(string $driver = '')
 * @method static Core set(array $params)                                                                               设置参数
 * @method static array clear()                                                                                         清空并且返回清空的数据
 * @method static Model|Collection increment(int $userId, int|string $wallet, int $financeType, float $amount, string $remark)
 * @method static Model|Collection decrement(int $userId, int|string $wallet, int $financeType, float $amount, string $remark)
 * @method static bool exists(int $userId, int|string $wallet)
 * @method static UserWallet initializeUserWallet(int $userId, int|string $wallet)
 * @method static void initializeAllUserWallet(int $userId)
 * @method static Collection getWalletConfig()
 * @method static Collection setWalletConfig(array $walletConfig)
 * @method static bool isInitializeWalletConfig()
 * @method static int getWalletCount()
 * @method static bool getWalletName(int|string $wallet)
 * @method static string hasWallet(int|string $wallet)
 * @method static float getWalletRatio(int|string $wallet)
 * @method static float getWalletBalance(int $userId, int|string $wallet)
 * @method static Collection getWalletBalances(int $userId, array $wallets)
 * @method static Collection getAllWalletBalances(int $userId)
 * @package GuanChanghu\Library\Facades
 */
class Wallet extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
