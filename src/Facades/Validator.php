<?php

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Repositories\Attribute as PaginationAttributeInterface;
use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\Validator as ValidatorContract;

/**
 * Class Validator
 * @method static ValidatorContract scene(string $scene)       注入场景
 * @method static Collection verify(...$params)       场景验证
 * @method static Collection validator(Collection $collect, array $rules, array $messages = [], ?Closure $closure = null)       验证
 * @method static Collection formValidator(Collection $form, array $rules, array $messages = [], ?Closure $closure = null)      form验证
 * @method static Collection conditionValidator(PaginationAttributeInterface $paginationAttribute, array $rules, array $messages = [], ?Closure $closure = null)        condition验证
 * @method static int|string|array formRequiredParam(Collection $form, string $key, array $otherRoles = [], mixed $default = null)      获取form里的参数，必填
 * @method static int|string|array paginationAttributeCollectionRequiredParam(PaginationAttributeInterface $paginationAttribute, string $key, array $otherRoles = [], mixed $default = null)        获取condition里的参数，必填
 * @method static int|string|array conditionRequiredParam(Collection $condition, string $key, array $otherRoles = [], mixed $default = null)        获取condition里的参数，必填
 * @method static int|string|array formOptionalParam(Collection $form, string $key, array $otherRoles = [], int|string|array|null $default = null)      获取form里的参数，不必填
 * @method static int|string|array paginationAttributeCollectionOptionalParam(PaginationAttributeInterface $paginationAttribute, string $key, array $otherRoles = [], int|string|array|null $default = null)        获取condition里的参数，不必填
 * @method static int|string|array conditionOptionalParam(Collection $condition, string $key, array $otherRoles = [], int|string|array|null $default = null)        获取condition里的参数，不必填
 * @package GuanChanghu\Library\Facades
 */
class Validator extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ValidatorContract::class;
    }
}
