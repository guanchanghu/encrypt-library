<?php


namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\ProjectInstall as ProjectInstallContract;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Facade;

/**
 * Class ProjectInstall
 * @method static void initialize(Command $command)
 * @method static void createDatabase(Command $command)
 * @method static void install(Command $command)
 * @method static void authorization(Command $command)
 * @method static void generateRsa()
 * @method static void updateRsa(string $key)
 * @package GuanChanghu\Library\Facades
 */
class ProjectInstall extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ProjectInstallContract::class;
    }
}
