<?php


namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Export as ExportContract;
use GuanChanghu\Library\Contracts\Repositories\Attribute as PaginateAttributeInterface;
use GuanChanghu\Library\Models\User;
use Illuminate\Support\Facades\Facade;

/**
 * Class Export
 * @method static string store(User $user, string $export, PaginateAttributeInterface $paginateAttribute, string $title, string $module = 'all');
 * @package GuanChanghu\Library\Facades
 */
class Export extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ExportContract::class;
    }
}
