<?php
/**
 * 短信门面
 * Created on 2022/4/27 16:05
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Sms\Core;
use GuanChanghu\Library\Contracts\Sms\Factory;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Sms
 * @package GuanChanghu\Library\Facades
 * @method static Core driver(string $driver = '')
 * @method static bool captcha(string $mobile, array $template, string $codeKey = '')
 * @method static void verify(string $mobile, string $captcha, string $key)
 * @method static bool checkout(string $mobile, string $captcha, string $key)
 * @method static bool notification(string $mobile, array $template)
 * Created on 2022/4/27 16:05
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Sms extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
