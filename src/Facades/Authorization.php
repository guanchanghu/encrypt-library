<?php


namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Authorization\Core;
use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\Authorization\Factory;

/**
 * @author 管昌虎
 * Class Authorization
 * @method static Core way(string $way = '')
 * @method static void validate()
 * @package GuanChanghu\Library\Facades
 * Created on 2022/8/12 11:20
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Authorization extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
