<?php
/**
 * 图片验证码门面
 * Created on 2022/4/22 13:51
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Captcha\Core as CoreContract;
use GuanChanghu\Library\Contracts\Captcha\Factory;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Captcha
 * @method static CoreContract way(string $way = '')
 * @method static array create(string $key = 'default')
 * @method static void verify(string $key, string $value)                      检验正确性并且删除cache
 * @method static bool checkout(string $key, string $value)                    只检验正确性不删除cache
 * @package GuanChanghu\Library\Facades
 * Created on 2022/4/22 13:51
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Captcha extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
