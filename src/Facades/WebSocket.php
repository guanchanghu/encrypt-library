<?php
/**
 * WebSocket
 * Created on 2022/8/22 17:59
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\WebSocket as WebSocketContract;
use GuanChanghu\Library\Models\User;
use Illuminate\Support\Facades\Facade;
use Swoole\Http\Request;

/**
 * @author 管昌虎
 * Class WebSocket
 * @method static string getClient(Request $request)
 * @method static User|null setUser(Request $request)
 * @method static array getUserByFd(int $fd)
 * @method static int getFdByUserId(int $userId)
 * @method static array getUserByUserId(int $userId)
 * @method static int getRoomProcessMake(int $roomId)
 * @method static void delUser(int $fd)
 * @method static string getRedisHashKey(int $roomId)
 * @method static void joinRedisRoom(int $roomId, int $userId)
 * @method static void outRedisRoom(int $roomId, int $userId)
 * @method static array getRedisRoomMember(int $roomId)
 * @package GuanChanghu\Library\Facades
 * Created on 2022/8/22 17:59
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class WebSocket extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return WebSocketContract::class;
    }
}
