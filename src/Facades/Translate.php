<?php


namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Translate as TranslateContract;
use Illuminate\Support\Facades\Facade;

/**
 * Class TranslateCommand
 * @method static bool package(string $form = 'zh', string $to = 'en', string $file = '')
 * @method static string|array translate(array|string $content, string $form = 'zh', string $to = 'en')
 * @package GuanChanghu\Library\Facades
 */
class Translate extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return TranslateContract::class;
    }
}
