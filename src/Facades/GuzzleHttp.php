<?php
/**
 * http门面
 * Created on 2022/8/16 17:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\GuzzleHttp as GuzzleHttpContract;
use Illuminate\Support\Facades\Facade;
use Closure;

/**
 * @author 管昌虎
 * Class GuzzleHttp
 * @method static GuzzleHttpContract setUrl(string $url)
 * @method static string getUrl()
 * @method static string clearUrl()
 * @method static GuzzleHttpContract setMethod(string $method)
 * @method static string getMethod()
 * @method static string clearMethod()
 * @method static GuzzleHttpContract setBody(string|array $body)
 * @method static string|array getBody()
 * @method static string|array clearBody()
 * @method static GuzzleHttpContract setBodyFormat(string $bodyFormat)
 * @method static string|array getBodyFormat()
 * @method static string|array clearBodyFormat()
 * @method static GuzzleHttpContract setHeaders(array $headers)
 * @method static GuzzleHttpContract putHeader(string $key, string $header)
 * @method static array getHeaders()
 * @method static array clearHeaders()
 * @method static GuzzleHttpContract setClientConfig(array $clientConfig)
 * @method static GuzzleHttpContract putClientConfig(string $key, string|array $clientConfig)
 * @method static array getClientConfig()
 * @method static array clearClientConfig()
 * @method static GuzzleHttpContract setLogs(array $logs)
 * @method static GuzzleHttpContract putLog(string $key, string $mode)
 * @method static array getLogs()
 * @method static array clearLogs()
 * @method static GuzzleHttpContract setBeforeCallback(Closure $beforeCallback)
 * @method static Closure|null getBeforeCallback()
 * @method static Closure|null clearBeforeCallback()
 * @method static GuzzleHttpContract setBeforeCallbackArgs(array $beforeCallbackArgs)
 * @method static array getBeforeCallbackArgs()
 * @method static array clearBeforeCallbackArgs()
 * @method static GuzzleHttpContract setAfterCallback(Closure $afterCallback)
 * @method static Closure|null getAfterCallback()
 * @method static Closure|null clearAfterCallback()
 * @method static GuzzleHttpContract setAfterCallbackArgs(array $afterCallbackArgs)
 * @method static array getAfterCallbackArgs()
 * @method static array clearAfterCallbackArgs()
 * @method static GuzzleHttpContract setRequestCount(int $requestCount)
 * @method static array getRequestCount()
 * @method static array clearRequestCount()
 * @method static GuzzleHttpContract setConcurrency(int $concurrency)
 * @method static array getConcurrency()
 * @method static array clearConcurrency()
 * @method static array|string|bool request(int $concurrency)
 * @package GuanChanghu\Library\Facades
 * Created on 2022/8/16 17:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class GuzzleHttp extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return GuzzleHttpContract::class;
    }
}
