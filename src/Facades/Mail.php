<?php
/**
 * 自定义邮件门面
 * Created on 2022/4/25 16:00
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Mail\Core;
use GuanChanghu\Library\Contracts\Mail\Factory;
use GuanChanghu\Library\Models\EmailAccount;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Mail\PendingMail;
use Illuminate\Support\Facades\Facade;

/**
 * @author 管昌虎
 * Class Mail
 * @method static Core mailer(string $name = '')
 * @method static PendingMail to($users)
 * @method static PendingMail bcc($users)
 * @method static void dispatch(string $to, Mailable $view, string $cc = '', string $bcc = '')
 * @method static EmailAccount|null accountSendNumUpdate(string $email)
 * @method static string generateCaptcha(string $email, string $key)
 * @method static void verifyCaptcha(string $email, string $captcha, string $key)
 * @package GuanChanghu\Library\Facades
 * Created on 2022/4/25 16:00
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Mail extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
