<?php
/**
 * 申请门面
 * Created on 2022/8/16 17:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use GuanChanghu\Library\Contracts\Exchange as ExchangeContract;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Class Exchange
 * @method static ExchangeContract pushItem(string $module, int $interface, array $form = [], array $list = [], int $version = 1)
 * @method static Collection getItems()
 * @method static array|string getResult(string $module, int $interface)
 * @method static Collection clearItems()
 * @method static Collection getHeaders()
 * @method static Collection clearHeaders()
 * @method static ExchangeContract putHeader(string $key, string $value)
 * @method static ExchangeContract setHeaders(array $headers)
 * @method static Collection getLogs()
 * @method static Collection clearLogs()
 * @method static ExchangeContract putLog(string $key, string $mode)
 * @method static bool getIsNeedToken()
 * @method static bool clearIsNeedToken()
 * @method static ExchangeContract setIsNeedToken(bool $isNeedToken)
 * @method static Collection request()
 * @package GuanChanghu\Library\Facades
 * Created on 2022/8/16 17:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Exchange extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ExchangeContract::class;
    }
}
