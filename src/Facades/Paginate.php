<?php

namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Contracts\Repositories\Core;
use GuanChanghu\Library\Contracts\Repositories\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Facade;

/**
 * Class Paginate
 * @method static Core way(string $way = '')
 * @method static int maxId(Builder $builder)
 * @method static int total(Builder $builder)
 * @method static array paginate(\GuanChanghu\Library\Contracts\Repositories\Query $paginate)
 * @package GuanChanghu\Library\Facades
 */
class Paginate extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
