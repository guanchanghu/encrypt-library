<?php


namespace GuanChanghu\Library\Facades;


use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\TerritoryLead as TerritoryLeadContract;

/**
 * Class Territory
 * @method static bool lead()
 * @package GuanChanghu\Library\Facades
 */
class TerritoryLead extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return TerritoryLeadContract::class;
    }
}
