<?php
/**
 * 模型查询门面
 * Created on 2022/4/18 9:50
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use GuanChanghu\Library\Contracts\ModelQuery as ModelQueryContract;

/**
 * @author 管昌虎
 * Class ModelQuery
 * @method static void queryTimeRange(Builder $builder, string $key, Collection $condition, string $filed = '')         根据时间范围查询数据,$filed 会自动在key和$filed选择,key 不会自动加上Range
 * @method static void queryRange(Builder $builder, string $key, Collection $condition, string $filed = '')             根据范围查询数据,key 自动加上Range
 * @method static void queryRangeByKey(Builder $builder, string $key, Collection $condition, string $filed = '')        根据范围查询数据,key 不加Range,$filed 不会在key和$filed选择
 * @method static void onlyQueryUserInfo(Builder $builder, string $key, Collection $condition)                          只查用户信息,可以指定是否精确搜索
 * @method static void queryUserInfo(Builder $builder, string $key, Collection $condition, string $relation = 'user')   查询用户信息
 * @method static void queryTimestampStatus(Builder $builder, string $key, Collection $condition, string $field = '', bool $reverse = true)     时间状态查询
 * @method static void queryIsStatus(Builder $builder, string $key, Collection $condition, bool $reverse = false)                     is 状态精确查询
 * @method static void queryCongruence(Builder $builder, string $key, Collection $condition)                            精确查询
 * @method static void queryOptional(Builder $builder, string $key, Collection $condition, array $fields = [])          精确或者模糊多字段查询
 * @method static void queryWithUser(Builder $builder, bool $withTrashed = false)                                       with user
 * @method static void queryUserByRelation(Builder $builder, string $relation, bool $withTrashed = false)               with user by relation
 * @method static Collection chunkWhereIn(Builder $builder, Collection $collection, string $field = '', array|Expression $wantField = ['*'])     分段查询whereIn
 * @package GuanChanghu\Library\Facades
 * Created on 2022/4/18 9:50
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class ModelQuery extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ModelQueryContract::class;
    }
}
