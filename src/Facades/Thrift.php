<?php
/**
 * thrift
 * Created on 2022/10/17 16:41
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\Thrift as ThriftContract;

/**
 * @author 管昌虎
 * Class Thrift
 * @method static ThriftContract setLocation(string $location = 'localhost')
 * @method static string getLocation()
 * @method static ThriftContract setPort(int $port = 9999)
 * @method static int getPort()
 * @method static ThriftContract setClient(string $client)
 * @method static string getClient()
 * @method static string clearClient()
 * @method static ThriftContract setModule(string $module)
 * @method static string getModule()
 * @method static string clearModule()
 * @method static ThriftContract setInterface(int $interface = 1000)
 * @method static int getInterface()
 * @method static int clearInterface()
 * @method static ThriftContract setUserId(int $userId = 0)
 * @method static int getUserId()
 * @method static int clearUserId()
 * @method static ThriftContract setVersion(int $version = 1)
 * @method static int getVersion()
 * @method static int clearVersion()
 * @method static ThriftContract setList(array $list = [])
 * @method static int getList()
 * @method static int clearList()
 * @method static ThriftContract setForm(array $form = [])
 * @method static int getForm()
 * @method static int clearForm()
 * @method static array getLogs()
 * @method static array clearLogs()
 * @method static ThriftContract putLog(string $key, string $mode)
 * @method static array|string moduleRequest()
 * @package GuanChanghu\Library\Facades
 * Created on 2022/10/17 16:47
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Thrift extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return ThriftContract::class;
    }
}
