<?php


namespace GuanChanghu\Library\Facades;


use GuanChanghu\Library\Models\RsaSecretKey;
use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\RSA\Factory;
use GuanChanghu\Library\Contracts\RSA\Core;

/**
 * Class RSA
 * @method static Core client(string $client)
 * @method static string encrypt(string $string)
 * @method static string decrypt(string $encrypt)
 * @method static string sign(array $array)
 * @method static bool|null verify(array $array)
 * @method static RsaSecretKey createKey(string $client, int $bits = 2048, array $params = [])
 * @method static RsaSecretKey updateKey(string $client)
 * @method static array generateKey(int $bits = 2048, bool $isDeleteFile = true)
 * @package GuanChanghu\Library\Facades
 */
class RSA extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Factory::class;
    }
}
