<?php
/**
 * 生成简单数学题
 * Created on 2022/1/14 14:44
 * Created by 管昌虎
 */

namespace GuanChanghu\Library\Facades;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Facade;
use GuanChanghu\Library\Contracts\SimpleMath as SimpleMathContract;
use Illuminate\Support\Collection;

/**
 * Class SimpleMath
 * @method static SimpleMathContract set(Collection $parameters)
 * @method static SimpleMathContract command(Command $command)
 * @method static Collection generate()
 * @method static array generateMath(float|int $currentResult, array $operatorProb, float $decimalProb, int $digitCurrent = 2)
 * @method static array generateFormula(array $operatorProb, float $currentResult, int $decimalProb = 0)
 * @method static float|int randomNumber(int $min, int $max, int $decimalProb = 0)
 * @package GuanChanghu\Library\Facades
 * Created on 2022/1/14 14:44
 * Created by 管昌虎
 */
class SimpleMath extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return SimpleMathContract::class;
    }
}
