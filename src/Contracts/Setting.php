<?php
/**
 * 环境设置
 * Created on 2022/8/26 8:41
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts;

/**
 * Class Setting
 * @package GuanChanghu\Library\Contracts
 * Created on 2022/8/26 8:42
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Setting
{
    /**
     * @return bool
     */
    public function isInitialize(): bool;

    /**
     * @return void
     */
    public function initializeSetting(): void;

    /**
     * @return string
     */
    public function getMacAddress(): string;
}
