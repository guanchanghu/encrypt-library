<?php


namespace GuanChanghu\Library\Contracts;


use GuanChanghu\Library\Contracts\Repositories\Attribute as PaginationAttributeInterface;
use GuanChanghu\Exceptions\DeveloperException;
use Closure;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

/**
 * Interface Validator
 * @package GuanChanghu\Library\Contracts
 */
interface Validator
{
    /**
     * 验证
     * @param Collection $collect
     * @param array $rules
     * @param array $messages
     * @param Closure|null $closure
     * @return Collection
     * @throws DeveloperException
     * @throws ValidationException
     */
    public function validator(Collection $collect, array $rules, array $messages = [], ?Closure $closure = null): Collection;

    /**
     * form验证
     * @param Collection $form
     * @param array $rules
     * @param array $messages
     * @param Closure|null $closure
     * @return Collection
     * @throws DeveloperException
     * @throws ValidationException
     */
    public function formValidator(Collection $form, array $rules, array $messages = [], ?Closure $closure = null): Collection;

    /**
     * condition验证
     * @param PaginationAttributeInterface $paginationAttribute
     * @param array $rules
     * @param array $messages
     * @param Closure|null $closure
     * @return Collection
     * @throws DeveloperException
     * @throws ValidationException
     */
    public function conditionValidator(PaginationAttributeInterface $paginationAttribute, array $rules, array $messages = [], ?Closure $closure = null): Collection;

    /**
     * 获取form里的参数，必填
     * @param Collection $form
     * @param string $key
     * @param array $otherRoles
     * @param mixed|null $default
     * @return mixed
     */
    public function formRequiredParam(Collection $form, string $key, array $otherRoles = [], mixed $default = null): mixed;

    /**
     * 获取condition里的参数，必填
     * @param Collection $condition
     * @param string $key
     * @param array $otherRoles
     * @param mixed|null $default
     * @return mixed
     */
    public function conditionRequiredParam(Collection $condition, string $key, array $otherRoles = [], mixed $default = null): mixed;

    /**
     * 获取condition里的参数，必填
     * @param PaginationAttributeInterface $paginationAttribute
     * @param string $key
     * @param array $otherRoles
     * @param mixed|null $default
     * @return mixed
     */
    public function paginationAttributeCollectionRequiredParam(PaginationAttributeInterface $paginationAttribute, string $key, array $otherRoles = [], mixed $default = null): mixed;

    /**
     * 获取form里的参数，不必填
     * @param Collection $form
     * @param string $key
     * @param array $otherRoles
     * @param mixed $default
     * @return mixed
     */
    public function formOptionalParam(Collection $form, string $key, array $otherRoles = [], mixed $default = null): mixed;

    /**
     * 获取condition里的参数，不必填
     * @param Collection $condition
     * @param string $key
     * @param array $otherRoles
     * @param mixed $default
     * @return mixed
     */
    public function conditionOptionalParam(Collection $condition, string $key, array $otherRoles = [], mixed $default = null): mixed;

    /**
     * 获取condition里的参数，不必填
     * @param PaginationAttributeInterface $paginationAttribute
     * @param string $key
     * @param array $otherRoles
     * @param mixed $default
     * @return mixed
     */
    public function paginationAttributeCollectionOptionalParam(PaginationAttributeInterface $paginationAttribute, string $key, array $otherRoles = [], mixed $default = null): mixed;

    /**
     * 注入场景
     * @param string $scene
     * @return Validator
     */
    public function scene(string $scene): Validator;

    /**
     * 场景验证
     * @param ...$params
     * @return Collection
     */
    public function verify(...$params): Collection;
}
