<?php


namespace GuanChanghu\Library\Contracts;

/**
 * Interface Territory
 * @package GuanChanghu\Library\Contracts\Lead
 */
interface TerritoryLead
{
    /**
     * @return bool
     */
    public function lead(): bool;
}
