<?php


namespace GuanChanghu\Library\Contracts;


use GuanChanghu\Exceptions\DeveloperException;
use GuanChanghu\Library\Models\User;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Role;

/**
 * Interface Permission
 * @package GuanChanghu\Library\Contracts
 */
interface Permission
{
    /**
     * @param string $client
     * @return Collection
     */
    public function getRolePermissions(string $client): Collection;

    /**
     * @param string $client
     * @param string $permission
     * @return string
     */
    public function resolveRoleByPermission(string $client, string $permission): string;

    /**
     * @return void
     * @throws DeveloperException
     */
    public function updateRole(): void;

    /**
     * @param User $user
     * @return void
     */
    public function updateUserPermission(User $user): void;

    /**
     * @param User $user
     * @param string $client
     * @param int $version
     * @param string $module
     * @return Role
     */
    public function assignRoleByModule(User $user, string $client, int $version, string $module): Role;

    /**
     * @param User $user
     * @param string $client
     * @return void
     */
    public function assignRoleByClient(User $user, string $client): void;

    /**
     * @param User $user
     * @param string $client
     * @param array $roles
     * @return void
     */
    public function assignRoles(User $user, string $client, array $roles): void;

    /**
     * @param User $user
     * @param string $client
     * @param int $version
     * @param string $module
     * @return Role
     */
    public function removeRoleByModule(User $user, string $client, int $version, string $module): Role;

    /**
     * @param int $version
     * @param Role $role
     */
    public function assignRolePermission(int $version, Role $role): void;
}
