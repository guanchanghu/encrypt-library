<?php


namespace GuanChanghu\Library\Contracts;

/**
 * Interface TranslateCommand
 * @package GuanChanghu\Library\Contracts
 */
interface Translate
{
    /**
     * @param string $form
     * @param string $to
     * @param string $file
     * @return bool
     */
    public function package(string $form = 'zh', string $to = 'en', string $file = ''): bool;

    /**
     * @param string|array $content
     * @param string $form
     * @param string $to
     * @return string|array
     */
    public function translate(array|string $content, string $form = 'zh', string $to = 'en'): array|string;
}
