<?php
/**
 * 短信契约支持
 * Created on 2022/4/27 16:01
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Sms;

/**
 * Class Support
 * @package GuanChanghu\Library\Contracts\Sms
 * Created on 2022/4/27 16:01
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Support
{
    /**
     * Support constructor.
     * @param array $config
     */
    public function __construct(array $config);

    /**
     * @param string|int $key
     * @return array|string|int|float|null
     */
    public function config(string|int $key = ''): array|string|int|float|null;

    /**
     * @param string $mobile
     * @param string $templateId
     * @param string $content
     * @param array $params
     * @return bool
     */
    public function send(string $mobile, string $templateId = '', string $content = '', array $params = []): bool;
}
