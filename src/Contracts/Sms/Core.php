<?php
/**
 * 短信契约核心
 * Created on 2022/4/27 16:00
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Sms;


/**
 * Class Core
 * @package GuanChanghu\Library\Contracts\Sms
 * Created on 2022/4/27 16:00
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Core
{
    /**
     * Core constructor.
     * @param Support $support
     */
    public function __construct(Support $support);

    /**
     * @param string $mobile
     * @param array $template
     * @param string $codeKey
     * @return bool
     */
    public function captcha(string $mobile, array $template, string $codeKey = ''): bool;

    /**
     * @param string $mobile
     * @param string $captcha
     * @param string $key
     * @return void
     */
    public function verify(string $mobile, string $captcha, string $key): void;

    /**
     * @param string $mobile
     * @param string $captcha
     * @param string $key
     * @return bool
     */
    public function checkout(string $mobile, string $captcha, string $key): bool;

    /**
     * @param string $mobile
     * @param array $template
     * @return bool
     */
    public function notification(string $mobile, array $template): bool;
}
