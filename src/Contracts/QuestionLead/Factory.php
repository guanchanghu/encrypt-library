<?php


namespace GuanChanghu\Library\Contracts\QuestionLead;

use Illuminate\Contracts\Foundation\Application;

/**
 * Interface Factory
 * @package GuanChanghu\Library\Contracts\Lead\Question
 */
interface Factory
{

    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * @param string $driver
     * @return Core
     */
    public function driver(string $driver = ''): Core;
}
