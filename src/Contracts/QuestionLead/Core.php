<?php

namespace GuanChanghu\Library\Contracts\QuestionLead;

use GuanChanghu\Exceptions\DeveloperException;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface Paginate
 * @package GuanChanghu\Library\Contracts\Repositories
 */
interface Core
{
    /**
     * @param string $file
     * @return $this
     */
    public function file(string $file): Core;

    /**
     * @return void
     */
    public function checkAll(): void;

    /**
     * @return void
     */
    public function lead(): void;

    /**
     * @param Collection $collection
     * @param int $parentId
     * @return void
     * @throws DeveloperException
     */
    public function lines(Collection $collection, int $parentId = 0): void;

    /**
     * @param Collection $collect
     * @param int $parentId
     * @return Model|null
     * @throws DeveloperException
     */
    public function line(Collection $collect, int $parentId = 0): Model|null;

    /**
     * @param string $toFile
     * @return void
     */
    public function txtToJson(string $toFile = ''): void;

    /**
     * @param string $type
     * @return void
     */
    public function checkTypeSegmentation(string $type): void;

    /**
     * @return void
     */
    public function checkAllTypeSegmentation(): void;

    /**
     * @return void
     */
    public function checkLine(): void;

    /**
     * @return void
     */
    public function checkStartString(): void;

    /**
     * @return void
     */
    public function checkTopicNumber(): void;

    /**
     * @return void
     */
    public function checkSubject(): void;

    /**
     * @return void
     */
    public function checkDifficulty(): void;
}
