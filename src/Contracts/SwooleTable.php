<?php

namespace GuanChanghu\Library\Contracts;

use Swoole\Table;

/**
 * Interface SwooleTable
 * @package GuanChanghu\Library\Contracts
 */
interface SwooleTable
{

    /**
     * SwooleTable constructor.
     * @param array $otherParam
     */
    public function __construct(array $otherParam = []);

    /**
     * @param array $otherParam
     * @return void
     */
    public function initialize(array $otherParam = []): void;

    /**
     * @param string $tableName
     */
    public function setTableName(string $tableName): void;

    /**
     * @return string
     */
    public function getTableName(): string;

    /**
     * 获得秒杀 swoole table key
     * @param string $identification
     * @return string
     */
    public function getSwooleTableKey(string $identification = ''): string;

    /**
     * @return Table
     */
    public function getSwooleTable(): Table;

    /**
     * @param string $identification
     * @return void
     */
    public function del(string $identification = ''): void;

    /**
     * @param string $identification
     * @return void
     */
    public function reset(string $identification = ''): void;

    /**
     * @return array
     */
    public function getSwooleTableField(): array;

    /**
     * @param string $identification
     * @param $field
     * @return int|array|string
     */
    public function get(string $identification = '', $field = null): int|array|string;

}
