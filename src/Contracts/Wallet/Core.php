<?php


namespace GuanChanghu\Library\Contracts\Wallet;

use GuanChanghu\Exceptions\DeveloperException;
use GuanChanghu\Library\Models\UserWallet;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Interface Facade
 * @package GuanChanghu\Library\Contracts\Models
 */
interface Core
{
    /**
     * 构造函数
     * Facade constructor.
     * @param Wallet $driver
     */
    public function __construct(Wallet $driver);

    /**
     * @param array $params
     * @return Core
     */
    public function set(array $params): Core;

    /**
     * @return array
     */
    public function clear(): array;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @param int $financeType
     * @param float $amount
     * @param string $remark
     * @return Model|Collection
     */
    public function increment(int $userId, int|string $wallet, int $financeType, float $amount, string $remark): Model|Collection;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @param int $financeType
     * @param float $amount
     * @param string $remark
     * @return Model|Collection
     */
    public function decrement(int $userId, int|string $wallet, int $financeType, float $amount, string $remark): Model|Collection;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @return bool
     */
    public function exists(int $userId, int|string $wallet): bool;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @return UserWallet
     */
    public function initializeUserWallet(int $userId, int|string $wallet): UserWallet;

    /**
     * @param int $userId
     * @return void
     */
    public function initializeAllUserWallet(int $userId): void;

    /**
     * @return bool
     */
    public function isInitializeWalletConfig(): bool;

    /**
     * @return Collection
     * @throws DeveloperException
     */
    public function getWalletConfig(): Collection;

    /**
     * @param array $walletConfig
     * @return Collection
     */
    public function setWalletConfig(array $walletConfig): Collection;

    /**
     * @return int
     */
    public function getWalletCount(): int;

    /**
     * @param int|string $wallet
     * @return string
     */
    public function getWalletName(int|string $wallet): string;

    /**
     * @param int|string $wallet
     * @return bool
     */
    public function hasWallet(int|string $wallet): bool;

    /**
     * @param int|string $wallet
     * @return float
     */
    public function getWalletRatio(int|string $wallet): float;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @return float
     */
    public function getWalletBalance(int $userId, int|string $wallet): float;

    /**
     * @param int $userId
     * @param array $wallets
     * @return Collection
     */
    public function getWalletBalances(int $userId, array $wallets): Collection;

    /**
     * @param int $userId
     * @return Collection
     */
    public function getAllWalletBalances(int $userId): Collection;
}
