<?php


namespace GuanChanghu\Library\Contracts\Wallet;

use Illuminate\Contracts\Foundation\Application;

/**
 * Interface Factory
 * @package GuanChanghu\Library\Contracts\Models
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * @param string $driver
     * @return Core
     */
    public function driver(string $driver): Core;
}
