<?php


namespace GuanChanghu\Library\Contracts\Wallet;

use GuanChanghu\Exceptions\DeveloperException;
use GuanChanghu\Library\Models\UserWallet;
use GuanChanghu\Library\Models\UserWalletLog;
use Illuminate\Support\Collection;

/**
 * Interface Models
 * @package GuanChanghu\Library\Contracts\Models
 */
interface Wallet
{
    /**
     * @param int $userId
     * @param int|string $wallet
     * @param int $financeType
     * @param float $amount
     * @param string $remark
     * @param array $params
     * @return UserWalletLog|Collection
     */
    public function increment(int $userId, int|string $wallet, int $financeType, float $amount, string $remark, array $params = []): UserWalletLog|Collection;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @param int $financeType
     * @param float $amount
     * @param string $remark
     * @param array $params
     * @return UserWalletLog|Collection
     */
    public function decrement(int $userId, int|string $wallet, int $financeType, float $amount, string $remark, array $params = []): UserWalletLog|Collection;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @param array $params
     * @return bool
     */
    public function exists(int $userId, int|string $wallet, array $params = []): bool;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @param array $params
     * @return UserWallet
     */
    public function initializeUserWallet(int $userId, int|string $wallet, array $params = []): UserWallet;

    /**
     * @return bool
     */
    public function isInitializeWalletConfig(): bool;

    /**
     * @param int $userId
     * @param array $params
     * @return void
     */
    public function initializeAllUserWallet(int $userId, array $params = []): void;

    /**
     * @return Collection
     * @throws DeveloperException
     */
    public function getWalletConfig(): Collection;

    /**
     * @param array $walletConfig
     * @return Collection
     * @throws DeveloperException
     */
    public function setWalletConfig(array $walletConfig): Collection;

    /**
     * @param int|string $wallet
     * @param array $params
     * @return array
     */
    public function getWalletItem(int|string $wallet, array $params = []): array;

    /**
     * @param int $userId
     * @param int|string $wallet
     * @param array $params
     * @return float
     */
    public function getWalletBalance(int $userId, int|string $wallet, array $params = []): float;

    /**
     * @param int $userId
     * @param array $wallets
     * @param array $params
     * @return Collection
     */
    public function getWalletBalances(int $userId, array $wallets, array $params = []): Collection;

    /**
     * @param int $userId
     * @param array $params
     * @return Collection
     */
    public function getAllWalletBalances(int $userId, array $params = []): Collection;
}
