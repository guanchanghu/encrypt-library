<?php


namespace GuanChanghu\Library\Contracts\Client;

use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;

/**
 * Interface Norm
 * @package GuanChanghu\Library\Contracts\Client
 */
interface Norm
{
    /**
     * Encrypt constructor.
     * @param string $client
     * @param Controller|null $controller
     */
    public function __construct(string $client, Controller|null $controller);

    /**
     * 模块名
     * @return string
     */
    public function moduleName(): string;

    /**
     * @return array
     */
    public static function interfaceCode(): array;

    /**
     * 接口列表
     * @return Collection
     */
    public function interfaceList(): Collection;

    /**
     * 不需要登录验证的接口
     * @return Collection
     */
    public function anonymityList(): Collection;

    /**
     * 是否需要事务
     * @return Collection
     */
    public function transactionList(): Collection;
}
