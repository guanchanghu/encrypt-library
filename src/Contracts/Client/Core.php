<?php

namespace GuanChanghu\Library\Contracts\Client;


use GuanChanghu\Library\Contracts\Client\Core as CoreContract;
use GuanChanghu\Exceptions\DeveloperException;
use Illuminate\Routing\Controller;
use GuanChanghu\Library\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use JsonException;

/**
 * Interface Facade
 * @package GuanChanghu\Library\Contracts\Client
 */
interface Core
{
    /**
     * Core constructor.
     * @param string $client
     */
    public function __construct(string $client);

    /**
     * 运行全部接口
     * @return array
     */
    public function run(): array;

    /**
     * @param User $user
     * @return Core
     */
    public function setUser(User $user): Core;

    /**
     * @param array $requestData
     * @return CoreContract
     */
    public function setRequestData(array $requestData): CoreContract;

    /**
     * @param Controller $controller
     * @return Core
     */
    public function setController(Controller $controller): Core;

    /**
     * 判断是否是重复访问
     * @param int $timestamp
     * @param int $random
     * @param User|null $user
     * @return void
     * @throws DeveloperException
     */
    public function throttle(int $timestamp, int $random, User|null $user = null): void;

    /**
     * 检查时间戳是否合法
     * @param int $millisecond
     * @throws DeveloperException
     */
    public function checkMillisecondTimestamp(int $millisecond): void;

    /**
     * @param string $module
     * @param int $interface
     * @param int $userId
     * @param int $version
     * @param array $list
     * @param array $form
     * @return array|string|Collection|Model
     */
    public function single(string $module, int $interface, int $userId = 0, int $version = 1, array $list = [], array $form = []): array|string|Collection|Model;

    /**
     * @return bool
     */
    public function clientPermissionSwitch(): bool;

    /**
     * @return array
     */
    public function moduleSwitch(): array;

    /**
     * @param int $version
     * @param string $module
     * @param int $interface
     * @return int
     */
    public function checkInterfaceCodeModule(int $version, string $module, int $interface): int;

    /**
     * 获得模块实例
     * @param int $version
     * @param string $module
     * @return Module
     */
    public function getModuleInstance(int $version, string $module): Module;

    /**
     * @param int $version
     * @return Collection
     */
    public function getAllModules(int $version = 0): Collection;

    /**
     * 获得平台默认不分配的模块
     * @return array
     */
    public function getDefaultForbidModule(): array;

    /**
     * @return int
     */
    public function getMaxVersion(): int;

    /**
     * 获得客户端名字
     * @return string
     */
    public function getClient(): string;

    /**
     * @return string
     */
    public function getUuid(): string;

    /**
     * @param string $client
     * @return bool
     */
    public function isClient(string $client): bool;
}
