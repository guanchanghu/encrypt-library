<?php


namespace GuanChanghu\Library\Contracts\Client;

use GuanChanghu\Exceptions\UserException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Foundation\Application;
use JsonException;

/**
 * Interface Factory
 * @package GuanChanghu\Library\Contracts\Client
 */
interface Factory
{

    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * 客户端
     * @param string $client
     * @return Core
     */
    public function client(string $client = ''): Core;

    /**
     * 获得用户默认平台列表
     * @return array
     */
    public function getClientDefault(): array;

    /**
     * 获得平台默认不分配的权限
     * @return array
     */
    public function getDefaultForbidPermission(): array;

    /**
     * @param string $client
     * @return int
     */
    public function getExpire(string $client): int;

    /**
     * @return array
     */
    public function getAllClient(): array;

    /**
     * @return array
     */
    public function getTreeClient(): array;

    /**
     * @return void
     */
    public function clearAllCache(): void;

    /**
     * @return array
     * @throws FileNotFoundException
     * @throws JsonException
     */
    public function getAllModuleSwitch(): array;

    /**
     * @param array $form
     * @return void
     * @throws UserException
     */
    public function setModuleSwitch(array $form): void;
}
