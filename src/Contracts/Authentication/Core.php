<?php
/**
 * 实人认证核心契约
 * Created on 2022/5/7 10:27
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Authentication;

use GuanChanghu\Library\Models\AuthenticationLog;
use GuanChanghu\Library\Models\User;

/**
 * Class Core
 * @package GuanChanghu\Library\Contracts\Authentication
 * Created on 2022/5/7 10:27
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Core
{
    /**
     * 构造函数
     * Core constructor.
     * @param Support $support
     */
    public function __construct(Support $support);

    /**
     * @param User $user
     * @param string $realName
     * @param string $idNumber
     * @param string $frontImage
     * @param string $backImage
     * @param string $handImage
     * @param array $params
     * @return array
     */
    public function token(User $user, string $realName, string $idNumber, string $frontImage = '', string $backImage = '', string $handImage = '', array $params = []): array;

    /**
     * @param User $user
     * @param string $realName
     * @param string $idNumber
     * @param string $orderNo
     * @param string $frontImage
     * @param string $backImage
     * @param string $handImage
     * @param array $params
     * @return AuthenticationLog
     */
    public function verify(User $user, string $realName = '', string $idNumber = '', string $orderNo = '', string $frontImage = '', string $backImage = '', string $handImage = '', array $params = []): AuthenticationLog;
}
