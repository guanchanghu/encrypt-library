<?php
/**
 * Thrift 发送
 * Created on 2022/10/17 10:21
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts;


/**
 * Class Thrift
 * @package GuanChanghu\Library\Contracts
 * Created on 2022/10/17 10:21
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Thrift
{
    /**
     * @param string $location
     * @return Thrift
     */
    public function setLocation(string $location = 'localhost'): Thrift;

    /**
     * @return string
     */
    public function getLocation(): string;

    /**
     * @param int $port
     * @return Thrift
     */
    public function setPort(int $port = 9999): Thrift;

    /**
     * @return int
     */
    public function getPort(): int;

    /**
     * @param string $key
     * @param string $mode
     * @return Thrift
     */
    public function putLog(string $key, string $mode): Thrift;

    /**
     * @return array
     */
    public function getLogs(): array;

    /**
     * @return array
     */
    public function clearLogs(): array;

    /**
     * @param string $client
     * @return Thrift
     */
    public function setClient(string $client): Thrift;

    /**
     * @return string
     */
    public function getClient(): string;

    /**
     * @return string
     */
    public function clearClient(): string;

    /**
     * @param string $module
     * @return Thrift
     */
    public function setModule(string $module): Thrift;

    /**
     * @return string
     */
    public function getModule(): string;

    /**
     * @return string
     */
    public function clearModule(): string;

    /**
     * @param int $interface
     * @return Thrift
     */
    public function setInterface(int $interface = 1000): Thrift;

    /**
     * @return int
     */
    public function getInterface(): int;

    /**
     * @return int
     */
    public function clearInterface(): int;

    /**
     * @param int $userId
     * @return Thrift
     */
    public function setUserId(int $userId = 0): Thrift;

    /**
     * @return int
     */
    public function getUserId(): int;

    /**
     * @return int
     */
    public function clearUserId(): int;

    /**
     * @param int $version
     * @return Thrift
     */
    public function setVersion(int $version = 1): Thrift;

    /**
     * @return int
     */
    public function getVersion(): int;

    /**
     * @return int
     */
    public function clearVersion(): int;

    /**
     * @param array $list
     * @return Thrift
     */
    public function setList(array $list = []): Thrift;

    /**
     * @return array
     */
    public function getList(): array;

    /**
     * @return array
     */
    public function clearList(): array;

    /**
     * @param array $form
     * @return Thrift
     */
    public function setForm(array $form = []): Thrift;

    /**
     * @return array
     */
    public function getForm(): array;

    /**
     * @return array
     */
    public function clearForm(): array;

    /**
     * @return array|string
     */
    public function moduleRequest(): array|string;
}
