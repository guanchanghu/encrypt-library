<?php
/**
 * 应用
 * Created on 2022/3/17 15:11
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Pay;

use GuanChanghu\Exceptions\UserException;
use GuanChanghu\Library\Models\UserAccount;
use Psr\Http\Message\ResponseInterface;
use Yansongda\Supports\Collection;

/**
 * Class App
 * @package GuanChanghu\Library\Contracts\Pay
 * Created on 2022/3/17 15:11
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface App
{
    /**
     * @param string $way
     * @param string $outTradeNo
     * @param float $amount
     * @param string $description
     * @param string $attach
     * @param array $other
     * @return ResponseInterface|Collection
     */
    public function pay(string $way, string $outTradeNo, float $amount, string $description, string $attach = '', array $other = array()): ResponseInterface|Collection;

    /**
     * @return Collection
     */
    public function callback(): Collection;

    /**
     * @param Collection $data
     * @return Collection
     * @throws UserException
     */
    public function checkSuccess(Collection $data): Collection;

    /**
     * @return ResponseInterface
     */
    public function success(): ResponseInterface;

    /**
     * @param string|array $order
     * @return array|Collection
     */
    public function find(string|array $order): array|Collection;

    /**
     * @param array $order
     * @return array|Collection
     * @throws \Yansongda\Pay\Exception\ContainerException
     * @throws \Yansongda\Pay\Exception\InvalidParamsException
     * @throws \Yansongda\Pay\Exception\ServiceNotFoundException
     */
    public function refund(array $order): array|Collection;

    /**
     * @param string|array $order
     * @return array|Collection
     */
    public function close(string|array $order): array|Collection;

    /**
     * @param string|array $order
     * @return array|Collection
     * @throws \Yansongda\Pay\Exception\ContainerException
     * @throws \Yansongda\Pay\Exception\InvalidParamsException
     * @throws \Yansongda\Pay\Exception\ServiceNotFoundException
     */
    public function cancel(string|array $order): array|Collection;

    /**
     * @param UserAccount $userAccount
     * @param string $outTradeNo
     * @param float $amount
     * @param string $title
     * @param string $remark
     * @return bool
     */
    public function transfer(UserAccount $userAccount, string $outTradeNo, float $amount, string $title, string $remark): bool;

    /**
     * @return bool
     */
    public function isConfig(): bool;
}
