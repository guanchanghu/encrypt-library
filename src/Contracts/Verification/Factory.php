<?php
/**
 * 人机验证工厂
 * Created on 2022/4/15 10:34
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Verification;

use Illuminate\Contracts\Foundation\Application;

/**
 * Class Factory
 * @package GuanChanghu\Library\Contracts\Verification
 * Created on 2022/4/15 10:36
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * 驱动
     * @param string driver
     * @return Core
     */
    public function driver(string $driver = ''): Core;
}
