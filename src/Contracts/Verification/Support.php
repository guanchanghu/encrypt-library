<?php
/**
 * 人机验证支持
 * Created on 2022/4/15 10:36
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Verification;

use GuanChanghu\Exceptions\UserException;

/**
 * Class Support
 * @package GuanChanghu\Library\Contracts\Verification
 * Created on 2022/4/15 10:36
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Support
{
    /**
     *
     */
    public function __construct(array $config);

    /**
     * @param array $params
     * @param string $way
     * @return bool
     */
    public function validate(array $params, string $way = ''): bool;

    /**
     * @param array $params
     * @param string $way
     * @return void
     */
    public function validateData(array $params, string $way = ''): void;
}
