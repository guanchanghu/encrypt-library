<?php
/**
 * 人机验证核心类
 * Created on 2022/4/15 10:35
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Verification;


/**
 * Class Core
 * @package GuanChanghu\Library\Contracts\Verification
 * Created on 2022/4/15 10:36
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Core
{
    /**
     * Core constructor.
     * @param Support $support
     */
    public function __construct(Support $support);

    /**
     * @param array $params
     * @param string $way
     * @return bool
     */
    public function validate(array $params, string $way = ''): bool;
}
