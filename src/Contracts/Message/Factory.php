<?php
/**
 * 消息工厂
 * Created on 2022/3/31 13:49
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Message;

use Illuminate\Contracts\Foundation\Application;

/**
 * Class Factory
 * @package GuanChanghu\Library\Contracts\Message
 * Created on 2022/3/31 13:49
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * @param string $driver
     * @return Core
     */
    public function driver(string $driver = ''): Core;
}
