<?php
/**
 * 消息实现类
 * Created on 2022/3/31 15:45
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Message;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Message
 * @package GuanChanghu\Library\Contracts\Message
 * Created on 2022/3/31 15:45
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Message
{
    /**
     * @param array $clients
     * @param int $userId
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @param array $params
     * @return Model
     */
    public function sendClientMany(array $clients, int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0, array $params = []): Model;

    /**
     * @param array $clients
     * @param Collection|array $userIds
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @param array $params
     * @return Model
     */
    public function sendMany(array $clients, Collection|array $userIds, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0, array $params = []): Model;

    /**
     * @param array $clients
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @param array $params
     * @return Model
     */
    public function sendAllClientMany(array $clients, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0, array $params = []): Model;

    /**
     * @param string $client
     * @param int $userId
     * @param array $params
     * @return void
     */
    public function supplyClientMany(string $client, int $userId, array $params = []): void;

    /**
     * @param int|string $massageId
     * @param int $userId
     * @param array $params
     * @return Model
     */
    public function getMessage(int|string $massageId, int $userId = 0, array $params = []): Model;

    /**
     * @param int|string $massageId
     * @param int $userId
     * @param array $params
     * @return string
     */
    public function jump(int|string $massageId, int $userId = 0, array $params = []): string;

    /**
     * @param int|string $massageId
     * @param int $userId
     * @param array $params
     * @return Model
     */
    public function read(int|string $massageId, int $userId = 0, array $params = []): Model;
}
