<?php
/**
 * 消息核心
 * Created on 2022/3/31 13:50
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Message;

use GuanChanghu\Library\Contracts\Repositories\Attribute as PaginateAttributeInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Class Core
 * @package GuanChanghu\Library\Contracts\Message
 * Created on 2022/3/31 13:50
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Core
{
    /**
     * 构造函数
     * Facade constructor.
     * @param Message $driver
     */
    public function __construct(Message $driver);

    /**
     * @param array $params
     * @return Core
     */
    public function set(array $params): Core;

    /**
     * @return array
     */
    public function clear(): array;

    /**
     * @param int $sendUserId
     * @param int $userId
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @return Model
     */
    public function send(int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0): Model;

    /**
     * @param string $client
     * @param int $userId
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @return Model
     */
    public function sendByClient(string $client, int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0): Model;


    /**
     * @param array $excludeClients
     * @param int $userId
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @return Model
     */
    public function sendExcludeClients(array $excludeClients, int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0): Model;

    /**
     * @param array $clients
     * @param int $userId
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @return Model
     */
    public function sendClientMany(array $clients, int $userId, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0): Model;

    /**
     * @param array $clients
     * @param Collection|array $userIds
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @return Model
     */
    public function sendMany(array $clients, Collection|array $userIds, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0): Model;

    /**
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @return Model
     */
    public function sendAll(string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0): Model;

    /**
     * @param array $clients
     * @param string $title
     * @param string $abstract
     * @param string $icon
     * @param string $content
     * @param int $sendUserId
     * @return Model
     */
    public function sendAllClientMany(array $clients, string $title, string $abstract, string $icon = '', string $content = '', int $sendUserId = 0): Model;

    /**
     * @param string $client
     * @param int $userId
     * @return void
     */
    public function supplyClientMany(string $client, int $userId): void;

    /**
     * @param int|string $massageId
     * @param int $userId
     * @return Model
     */
    public function getMessage(int|string $massageId, int $userId = 0): Model;

    /**
     * @param int|string $massageId
     * @param int $userId
     * @return string
     */
    public function jump(int|string $massageId, int $userId = 0): string;

    /**
     * @param int|string $massageId
     * @param int $userId
     * @return Model
     */
    public function read(int|string $massageId, int $userId = 0): Model;

    /**
     * @param PaginateAttributeInterface $paginateAttribute
     * @return array
     */
    #[ArrayShape(['total' => "int", 'perPage' => "int", 'currentPage' => "int", 'lastPage' => "float", 'list' => "\array|\Illuminate\Support\Collection"])] public function pageList(PaginateAttributeInterface $paginateAttribute): array;

    /**
     * @param int|string $messageId
     * @param PaginateAttributeInterface $paginateAttribute
     * @return array
     */
    #[ArrayShape(['total' => "int", 'perPage' => "int", 'currentPage' => "int", 'lastPage' => "float", 'list' => "\array|\Illuminate\Support\Collection"])] public function showMessageUserPageList(int|string $messageId, PaginateAttributeInterface $paginateAttribute): array;
}
