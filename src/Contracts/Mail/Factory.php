<?php
/**
 * 邮件发送工厂
 * Created on 2022/4/25 13:30
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Mail;

use Illuminate\Contracts\Foundation\Application;

/**
 * Class Factory
 * @package GuanChanghu\Library\Contracts\Mail
 * Created on 2022/4/27 9:49
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * @param string $name
     * @return Core
     */
    public function mailer(string $name = ''): Core;
}
