<?php
/**
 * 邮件发送工厂
 * Created on 2022/4/25 13:30
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Mail;

use GuanChanghu\Library\Models\EmailAccount;
use Illuminate\Contracts\Mail\Mailable;

/**
 * Class Core
 * @package GuanChanghu\Library\Contracts\Mail
 * Created on 2022/4/27 9:50
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Core
{
    /**
     * Core constructor.
     * @param string $name
     */
    public function __construct(string $name);

    /**
     * @param string $to
     * @param Mailable $view
     * @param string $cc
     * @param string $bcc
     * @return void
     */
    public function dispatch(string $to, Mailable $view, string $cc = '', string $bcc = ''): void;


    /**
     * @param string $email
     * @return \GuanChanghu\Library\Services\Mail\Models\EmailAccount|null
     */
    public function accountSendNumUpdate(string $email): EmailAccount|null;

    /**
     * @param string $email
     * @param string $key
     * @return string
     */
    public function generateCaptcha(string $email, string $key): string;

    /**
     * @param string $email
     * @param string $captcha
     * @param string $key
     * @return void
     */
    public function verifyCaptcha(string $email, string $captcha, string $key): void;
}
