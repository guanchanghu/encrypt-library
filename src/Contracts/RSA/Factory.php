<?php


namespace GuanChanghu\Library\Contracts\RSA;


use GuanChanghu\Exceptions\DeveloperException;
use GuanChanghu\Library\Models\RsaSecretKey;
use Illuminate\Contracts\Foundation\Application;
use Seffeng\Cryptlib\Exceptions\CryptException;

/**
 * Class Factory
 * @package GuanChanghu\Library\Contracts\RSA
 * Created on 2022/8/8 16:03
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * @param string $client
     * @return Core
     */
    public function client(string $client): Core;

    /**
     * @param string $client
     * @param int $bits
     * @param array $params
     * @return RsaSecretKey
     */
    public function createKey(string $client, int $bits = 2048, array $params = []): RsaSecretKey;

    /**
     * @param int $bits
     * @param bool $isDeleteFile
     * @return array
     */
    public function generateKey(int $bits = 2048, bool $isDeleteFile = true): array;

    /**
     * @param string $client
     * @return RsaSecretKey
     * @throws CryptException
     * @throws DeveloperException
     */
    public function updateKey(string $client): RsaSecretKey;
}
