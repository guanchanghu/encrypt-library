<?php


namespace GuanChanghu\Library\Contracts\RSA;

use GuanChanghu\Library\Models\RsaSecretKey;

/**
 * Class Core
 * @package GuanChanghu\Library\Contracts\RSA
 * Created on 2022/7/26 16:16
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Core
{
    /**
     * Core constructor.
     * @param RsaSecretKey $config
     */
    public function __construct(RsaSecretKey $config);

    /**
     * @param string $string
     * @return string
     */
    public function encrypt(string $string): string;

    /**
     * @param string $encrypt
     * @return string
     */
    public function decrypt(string $encrypt): string;

    /**
     * @param array $array
     * @return string
     */
    public function sign(array $array): string;

    /**
     * @param array $array
     * @return bool|null
     */
    public function verify(array $array): ?bool;
}
