<?php
/**
 * 图片验证码支持契约
 * Created on 2022/4/22 15:13
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Captcha;


/**
 * Class Support
 * @package GuanChanghu\Library\Contracts\Captcha
 * Created on 2022/4/22 15:13
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Support
{
    /**
     * @param array $config
     * @return array
     */
    public function createCaptcha(array $config): array;

    /**
     * @param array $config
     * @return string|null
     */
    public function font(array $config): string|null;
}
