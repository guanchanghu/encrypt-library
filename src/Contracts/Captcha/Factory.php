<?php
/**
 * 图片验证码工厂契约
 * Created on 2022/4/22 15:11
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Captcha;

use Illuminate\Contracts\Foundation\Application;

/**
 * Class Factory
 * @package GuanChanghu\Library\Contracts\Captcha
 * Created on 2022/4/22 15:12
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * @param string $way
     * @return Core
     */
    public function way(string $way = ''): Core;
}
