<?php
/**
 * 第三方授权
 * Created on 2022/4/12 10:14
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts;

use GuanChanghu\Library\Models\User;
use GuanChanghu\Library\Models\UserSocialite;

/**
 * Class Socialite
 * @package GuanChanghu\Library\Contracts
 * Created on 2022/4/12 10:15
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Socialite
{
    /**
     * @param string $code
     * @param User|null $user
     * @param array $clients
     * @return \GuanChanghu\Library\Services\Socialite\Models\UserSocialite
     */
    public function weChat(string $code, User|null $user = null, array $clients = []): UserSocialite;
}
