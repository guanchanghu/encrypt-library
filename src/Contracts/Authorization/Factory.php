<?php


namespace GuanChanghu\Library\Contracts\Authorization;


use Illuminate\Contracts\Foundation\Application;

/**
 * Interface Factory
 * @package GuanChanghu\Library\Contracts\AuthorizationConfig
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * 方式
     * @param string $way
     * @return Core
     */
    public function way(string $way = ''): Core;
}
