<?php
/**
 * http请求
 * Created on 2022/8/16 16:24
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts;

use Closure;

/**
 * Class GuzzleHttp
 * @package GuanChanghu\Library\Contracts
 * Created on 2022/8/16 16:24
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface GuzzleHttp
{
    /**
     * @param string $url
     * @return GuzzleHttp
     */
    public function setUrl(string $url): GuzzleHttp;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return string
     */
    public function clearUrl(): string;

    /**
     * @param string $method
     * @return GuzzleHttp
     */
    public function setMethod(string $method): GuzzleHttp;

    /**
     * @return string
     */
    public function getMethod(): string;

    /**
     * @return string
     */
    public function clearMethod(): string;

    /**
     * @param string|array $body
     * @return GuzzleHttp
     */
    public function setBody(string|array $body): GuzzleHttp;

    /**
     * @return string|array
     */
    public function getBody(): string|array;

    /**
     * @return string|array
     */
    public function clearBody(): string|array;

    /**
     * @param string $bodyFormat
     * @return GuzzleHttp
     */
    public function setBodyFormat(string $bodyFormat): GuzzleHttp;

    /**
     * @return string
     */
    public function getBodyFormat(): string;

    /**
     * @return string
     */
    public function clearBodyFormat(): string;

    /**
     * @param array $headers
     * @return GuzzleHttp
     */
    public function setHeaders(array $headers): GuzzleHttp;

    /**
     * @param string $key
     * @param string $header
     * @return GuzzleHttp
     */
    public function putHeader(string $key, string $header): GuzzleHttp;

    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @return array
     */
    public function clearHeaders(): array;

    /**
     * @param array $clientConfig
     * @return GuzzleHttp
     */
    public function setClientConfig(array $clientConfig): GuzzleHttp;

    /**
     * @param string $key
     * @param string|array $clientConfig
     * @return GuzzleHttp
     */
    public function putClientConfig(string $key, string|array $clientConfig): GuzzleHttp;

    /**
     * @return array
     */
    public function getClientConfig(): array;

    /**
     * @return array
     */
    public function clearClientConfig(): array;


    /**
     * @param array $logs
     * @return GuzzleHttp
     */
    public function setLogs(array $logs): GuzzleHttp;

    /**
     * @param string $key
     * @param string $mode
     * @return GuzzleHttp
     */
    public function putLog(string $key, string $mode): GuzzleHttp;

    /**
     * @return array
     */
    public function getLogs(): array;

    /**
     * @return array
     */
    public function clearLogs(): array;


    /**
     * @param Closure $beforeCallback
     * @return GuzzleHttp
     */
    public function setBeforeCallback(Closure $beforeCallback): GuzzleHttp;

    /**
     * @return Closure|null
     */
    public function getBeforeCallback(): Closure|null;

    /**
     * @return Closure|null
     */
    public function clearBeforeCallback(): Closure|null;

    /**
     * @param array $beforeCallbackArgs
     * @return GuzzleHttp
     */
    public function setBeforeCallbackArgs(array $beforeCallbackArgs): GuzzleHttp;

    /**
     * @return array
     */
    public function getBeforeCallbackArgs(): array;

    /**
     * @return array
     */
    public function clearBeforeCallbackArgs(): array;

    /**
     * @param Closure $afterCallback
     * @return GuzzleHttp
     */
    public function setAfterCallback(Closure $afterCallback): GuzzleHttp;

    /**
     * @return Closure|null
     */
    public function getAfterCallback(): Closure|null;

    /**
     * @return Closure|null
     */
    public function clearAfterCallback(): Closure|null;

    /**
     * @param array $afterCallbackArgs
     * @return GuzzleHttp
     */
    public function setAfterCallbackArgs(array $afterCallbackArgs): GuzzleHttp;

    /**
     * @return array
     */
    public function getAfterCallbackArgs(): array;

    /**
     * @return array
     */
    public function clearAfterCallbackArgs(): array;


    /**
     * @param int $requestCount
     * @return GuzzleHttp
     */
    public function setRequestCount(int $requestCount): GuzzleHttp;

    /**
     * @return int
     */
    public function getRequestCount(): int;

    /**
     * @return int
     */
    public function clearRequestCount(): int;

    /**
     * @param int $concurrency
     * @return GuzzleHttp
     */
    public function setConcurrency(int $concurrency): GuzzleHttp;

    /**
     * @return int
     */
    public function getConcurrency(): int;

    /**
     * @return int
     */
    public function clearConcurrency(): int;

    /**
     * @return array|bool
     */
    public function request(): array|bool;
}
