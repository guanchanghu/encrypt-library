<?php


namespace GuanChanghu\Library\Contracts\Upload;

use Illuminate\Contracts\Foundation\Application;

/**
 * Interface Factory
 * @package GuanChanghu\Library\Contracts\Upload
 */
interface Factory
{

    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * 驱动
     * @param string driver
     * @return Core
     */
    public function driver(string $driver = ''): Core;
}
