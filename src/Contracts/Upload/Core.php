<?php

namespace GuanChanghu\Library\Contracts\Upload;

use GuanChanghu\Library\Models\FileStoreRecord;
use GuanChanghu\Library\Models\User;
use Illuminate\Http\Request;

/**
 * Interface Facade
 * @package GuanChanghu\Library\Contracts\Upload
 */
interface Core
{
    /**
     * @param User $user
     * @param Request $request
     * @return FileStoreRecord
     */
    public function upload(User $user, Request $request): FileStoreRecord;

    /**
     * @param User $user
     * @param string $sourceFile
     * @param string $client
     * @param string $module
     * @return FileStoreRecord
     */
    public function copy(User $user, string $sourceFile, string $client = '', string $module = 'all'): FileStoreRecord;
}
