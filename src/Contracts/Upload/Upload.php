<?php


namespace GuanChanghu\Library\Contracts\Upload;


use GuanChanghu\Library\Models\User;
use Illuminate\Http\Request;

interface Upload
{
    /**
     * @param User $user
     * @param Request $request
     * @return array
     */
    public function upload(User $user, Request $request): array;

    /**
     * @param User $user
     * @param string $sourceFile
     * @param string $client
     * @param string $module
     * @return array
     */
    public function copy(User $user, string $sourceFile, string $client = '', string $module = 'all'): array;
}
