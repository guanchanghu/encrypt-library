<?php
/**
 * 生成简单数学题
 * Created on 2022/1/13 17:15
 * Created by 管昌虎
 */

namespace GuanChanghu\Library\Contracts;

use GuanChanghu\Exceptions\DeveloperException;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Exception;

/**
 * Created on 2022/1/14 8:25
 * Created by 管昌虎
 * Class SimpleMath
 * @package GuanChanghu\Library\Contracts
 */
interface SimpleMath
{
    /**
     * @param Collection $parameters
     * @return SimpleMath
     */
    public function set(Collection $parameters): SimpleMath;

    /**
     * @param Command $command
     * @return SimpleMath
     */
    public function command(Command $command): SimpleMath;

    /**
     * @return Collection
     */
    public function generate(): Collection;

    /**
     * @param float|int $currentResult
     * @param array $operatorProb
     * @param float $decimalProb
     * @param int $digitCurrent
     * @return array
     * @throws DeveloperException
     * @throws Exception
     */
    public function generateMath(float|int $currentResult, array $operatorProb, float $decimalProb, int $digitCurrent = 2): array;

    /**
     * @param array $operatorProb
     * @param float|int $currentResult
     * @param int $decimalProb
     * @return array
     */
    public function generateFormula(array $operatorProb, float|int $currentResult, int $decimalProb = 0): array;

    /**
     * @param int $min
     * @param int $max
     * @param int $decimalProb
     * @return float|int
     * @throws Exception
     */
    public function randomNumber(int $min, int $max, int $decimalProb = 0): float|int;
}
