<?php

namespace GuanChanghu\Library\Contracts\Log;


/**
 * @author 管昌虎
 * Interface Factory
 * @package GuanChanghu\Library\Contracts\Log
 * Created on 2023/4/5 09:03
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface Factory
{
    /**
     * @param string|null $channel
     * @return Factory
     */
    public function channel(string $channel = null): Factory;

    /**
     * @return string
     */
    public function clearChannel(): string;

    /**
     * @param string $client
     * @return Factory
     */
    public function setClient(string $client): Factory;

    /**
     * @return string
     */
    public function clearClient(): string;

    /**
     * @param string $queue
     * @return Factory
     */
    public function onQueue(string $queue): Factory;

    /**
     * @return string
     */
    public function clearQueue(): string;

    /**
     * @param int $logType
     * @return Factory
     */
    public function setLogType(int $logType): Factory;

    /**
     * @return int
     */
    public function clearLogType(): int;

    /**
     * @param bool $isSlowLog
     * @return Factory
     */
    public function setSlowLog(bool $isSlowLog): Factory;

    /**
     * @return bool
     */
    public function clearSlowLog(): bool;

    /**
     * @param int $delay
     * @return Factory
     */
    public function setDelay(int $delay): Factory;

    /**
     * @return int
     */
    public function clearDelay(): int;

    /**
     * @param string $message
     * @param array $context
     */
    public function alert(string $message, array $context = []): void;

    /**
     * @param string $message
     * @param array $context
     */
    public function critical(string $message, array $context = []): void;

    /**
     * @param string $message
     * @param array $context
     */
    public function debug(string $message, array $context = []): void;

    /**
     * @param string $message
     * @param array $context
     */
    public function emergency(string $message, array $context = []): void;

    /**
     * @param string $message
     * @param array $context
     */
    public function error(string $message, array $context = []): void;

    /**
     * @param string $message
     * @param array $context
     */
    public function info(string $message, array $context = []): void;

    /**
     * @param $level
     * @param string $message
     * @param array $context
     */
    public function log($level, string $message, array $context = []): void;

    /**
     * @param string $message
     * @param array $context
     */
    public function notice(string $message, array $context = []): void;

    /**
     * @param string $message
     * @param array $context
     */
    public function warning(string $message, array $context = []): void;

    /**
     * @param string $level
     * @param string $message
     * @param array $context
     */
    public function write(string $level, string $message, array $context = []): void;
}
