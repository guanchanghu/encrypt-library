<?php
/**
 * WebSocket
 * Created on 2022/8/22 17:58
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts;

use GuanChanghu\Library\Models\User as UserModel;
use Swoole\Http\Request;

/**
 * Class WebSocket
 * @package GuanChanghu\Library\Contracts
 * Created on 2022/8/22 17:59
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface WebSocket
{
    /**
     * @param Request $request
     * @return string
     */
    public function getClient(Request $request): string;

    /**
     * @param Request $request
     * @return UserModel|null
     */
    public function setUser(Request $request): UserModel|null;

    /**
     * @param int $fd
     * @return array
     */
    public function getUserByFd(int $fd): array;

    /**
     * @param int $userId
     * @return int
     */
    public function getFdByUserId(int $userId): int;

    /**
     * @param int $userId
     * @return array
     */
    public function getUserByUserId(int $userId): array;

    /**
     * @param int $fd
     * @return void
     */
    public function delUser(int $fd): void;

    /**
     * @param int $roomId
     * @return int
     */
    public function getRoomProcessMake(int $roomId): int;

    /**
     * @param int $roomId
     * @return string
     */
    public function getRedisHashKey(int $roomId): string;

    /**
     * @param int $roomId
     * @param int $userId
     * @return void
     */
    public function joinRedisRoom(int $roomId, int $userId): void;

    /**
     * @param int $roomId
     * @param int $userId
     * @return void
     */
    public function outRedisRoom(int $roomId, int $userId): void;

    /**
     * @param int $roomId
     * @return array
     */
    public function getRedisRoomMember(int $roomId): array;
}
