<?php
/**
 * 模型查询
 * Created on 2022/4/18 9:24
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts;

use Illuminate\Database\Query\Expression;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ModelQuery
 * @package GuanChanghu\Library\Contracts
 * Created on 2022/4/18 9:25
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface ModelQuery
{
    /**
     * 根据时间范围查询数据,$filed 会自动在key和$filed选择
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     * @param string $filed
     */
    public function queryTimeRange(Builder $builder, string $key, Collection $condition, string $filed = ''): void;


    /**
     * 根据范围查询数据,key 自动加上Range
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     * @param string $filed
     */
    public function queryRange(Builder $builder, string $key, Collection $condition, string $filed = ''): void;

    /**
     * 根据范围查询数据,key 不加Range,$filed 不会在key和$filed选择
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     * @param string $filed
     * @return void
     */
    public function queryRangeByKey(Builder $builder, string $key, Collection $condition, string $filed = ''): void;

    /**
     * 只查用户信息,可以指定是否精确搜索
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     */
    public function onlyQueryUserInfo(Builder $builder, string $key, Collection $condition): void;

    /**
     * 查询用户信息
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     * @param string $relation
     */
    public function queryUserInfo(Builder $builder, string $key, Collection $condition, string $relation = 'user'): void;

    /**
     * 时间状态查询
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     * @param string $field
     * @param bool $reverse
     */
    public function queryTimestampStatus(Builder $builder, string $key, Collection $condition, string $field = '', bool $reverse = true): void;

    /**
     * is 状态精确查询
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     * @param bool $reverse
     * @return void
     */
    public function queryIsStatus(Builder $builder, string $key, Collection $condition, bool $reverse = false): void;

    /**
     * 精确查询
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     */
    public function queryCongruence(Builder $builder, string $key, Collection $condition): void;

    /**
     * 精确或者模糊多字段查询
     * @param Builder $builder
     * @param string $key
     * @param Collection $condition
     * @param array $fields
     */
    public function queryOptional(Builder $builder, string $key, Collection $condition, array $fields = []): void;

    /**
     * with user
     * @param Builder $builder
     * @param bool $withTrashed
     */
    public function queryWithUser(Builder $builder, bool $withTrashed = false): void;

    /**
     * with user by relation
     * @param Builder $builder
     * @param string $relation
     * @param bool $withTrashed
     */
    public function queryUserByRelation(Builder $builder, string $relation, bool $withTrashed = false): void;

    /**
     * 分段查询whereIn
     * @param Builder $builder
     * @param Collection $collection
     * @param string $field
     * @param array|Expression|string[] $wantField
     * @return Collection
     */
    public function chunkWhereIn(Builder $builder, Collection $collection, string $field = '', array|Expression $wantField = ['*']): Collection;
}
