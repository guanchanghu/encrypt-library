<?php


namespace GuanChanghu\Library\Contracts;


use GuanChanghu\Library\Contracts\Repositories\Attribute as PaginateAttributeInterface;
use GuanChanghu\Library\Models\User;

interface Export
{
    /**
     * @param User $user
     * @param string $export
     * @param PaginateAttributeInterface $paginateAttribute
     * @param string $title
     * @param string $module
     * @return string
     */
    public function store(User $user, string $export, PaginateAttributeInterface $paginateAttribute, string $title, string $module = 'all'): string;
}
