<?php
/**
 * 授权工厂
 * Created on 2022/4/6 13:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Auth;

use Illuminate\Contracts\Foundation\Application;

/**
 * Class Factory
 * @package GuanChanghu\Library\Contracts\Auth
 * Created on 2022/4/6 13:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Factory
{
    /**
     * Factory constructor.
     * @param Application $app
     */
    public function __construct(Application $app);

    /**
     * @param string $way
     * @return Core
     */
    public function way(string $way = ''): Core;
}
