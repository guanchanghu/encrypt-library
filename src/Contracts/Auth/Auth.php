<?php
/**
 * 授权实现类
 * Created on 2022/4/6 13:16
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Auth;

use GuanChanghu\Library\Models\User;

/**
 * Class Auth
 * @package GuanChanghu\Library\Contracts\Auth
 * Created on 2022/4/6 13:16
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Auth
{
    /**
     * @param User $user
     * @param string $client
     * @param int $seconds
     * @return array
     */
    public function createBearerToken(User $user, string $client, int $seconds): array;

    /**
     * @param int $userId
     * @param string $client
     * @return bool
     */
    public function isOnline(int $userId, string $client): bool;

    /**
     * @param User $user
     * @param string $client
     * @return void
     */
    public function logout(User $user, string $client): void;

    /**
     * @param string $client
     * @param string $authorization
     * @return User
     */
    public function resolve(string $client, string $authorization): User;

    /**
     * @return void
     */
    public function expire(): void;
}
