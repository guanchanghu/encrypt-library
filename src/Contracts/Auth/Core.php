<?php
/**
 * 授权核心
 * Created on 2022/4/6 13:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts\Auth;

use GuanChanghu\Library\Models\User;

/**
 * Class Core
 * @package GuanChanghu\Library\Contracts\Auth
 * Created on 2022/4/6 13:10
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Core
{
    /**
     * Core constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth);

    /**
     * @param User $user
     * @param string $client
     * @param int $seconds
     * @return array
     */
    public function createBearerToken(User $user, string $client, int $seconds): array;

    /**
     * @param int $userId
     * @param string $client
     * @return bool
     */
    public function isOnline(int $userId, string $client): bool;

    /**
     * @param User $user
     * @param string $client
     * @return void
     */
    public function logout(User $user, string $client): void;

    /**
     * @param string $client
     * @return User|null
     */
    public function resolve(string $client): User|null;

    /**
     * @return void
     */
    public function expire(): void;
}
