<?php

namespace GuanChanghu\Library\Contracts\Repositories;


use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Interface QueryRenderAfter
 * @package GuanChanghu\Library\Contracts\Repositories
 * Created on 2023/4/13 21:20
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface QueryRenderAfter
{
    /**
     * @param Collection|array $list
     * @return Collection|array
     */
    public function after(Collection|array $list): Collection|array;
}
