<?php

namespace GuanChanghu\Library\Contracts\Repositories;

use Illuminate\Database\Eloquent\Builder;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Interface Facade
 * @package GuanChanghu\Library\Contracts\Repositories
 */
interface Core
{
    /**
     * @param Support $support
     */
    public function __construct(Support $support);

    /**
     * 获得最大ID
     * @param Builder $builder
     * @return int
     */
    public function maxId(Builder $builder): int;

    /**
     * 获取分页总数
     * @param Builder $builder
     * @return int
     */
    public function total(Builder $builder): int;

    /**
     * @param Query $query
     * @return array
     */
    #[ArrayShape(['total' => "int", 'perPage' => "int", 'currentPage' => "int", 'lastPage' => "float", 'list' => "array|\Illuminate\Support\Collection"])] public function paginate(Query $query): array;
}
