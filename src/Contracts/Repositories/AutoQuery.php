<?php

namespace GuanChanghu\Library\Contracts\Repositories;


/**
 * @author 管昌虎
 * Interface AutoQuery
 * @package GuanChanghu\Library\Contracts\Repositories
 * Created on 2023/4/15 09:47
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface AutoQuery
{
    /**
     * 自动查询
     */
    public function autoQuery(): Query;
}
