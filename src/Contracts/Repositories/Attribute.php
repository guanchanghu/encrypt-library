<?php

namespace GuanChanghu\Library\Contracts\Repositories;


use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Interface Attribute
 * @package GuanChanghu\Library\Contracts\Repositories
 */
interface Attribute
{
    /**
     * @param Request|Collection|array $request
     */
    public function __construct(Request|Collection|array $request = []);

    /**
     * @param array $attribute
     * @return Collection
     */
    public function setAttribute(array $attribute): Collection;

    /**
     * @return Collection
     */
    public function attribute(): Collection;

    /**
     * @return int
     */
    public function page(): int;

    /**
     * @return int
     */
    public function paginate(): int;

    /**
     * 获得查询条件,传递的条件会临时融合返回
     * @return Collection
     */
    public function condition(): Collection;

    /**
     * 获得查询条件,传递的条件会融合返回
     * @param array $condition
     * @return Collection
     */
    public function mergeCondition(array $condition): Collection;

    /**
     * 获得排序条件,传递的排序会临时融合返回
     * @return Collection
     */
    public function sorter(): Collection;

    /**
     * 获得排序条件,传递的排序会融合返回
     * @param array $sorter
     * @return Collection
     */
    public function mergeSorter(array $sorter): Collection;

    /**
     * 返回参数
     * @return Collection
     */
    public function backParameter(): Collection;

    /**
     * 返回参数
     * @param string $key
     * @param $value
     * @return Collection
     */
    public function setBackParameter(string $key, $value): Collection;

    /**
     * 回调参数
     * @return Collection
     */
    public function transmitParameter(): Collection;

    /**
     * 传递参数
     * @param string $key
     * @param $value
     * @return Collection
     */
    public function setTransmitParameter(string $key, $value): Collection;
}
