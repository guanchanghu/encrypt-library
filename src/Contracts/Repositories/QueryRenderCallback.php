<?php

namespace GuanChanghu\Library\Contracts\Repositories;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Interface QueryRenderCallback
 * @package GuanChanghu\Library\Contracts\Repositories
 * Created on 2023/4/13 21:19
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface QueryRenderCallback
{
    /**
     * @param Model $model
     * @return array|Collection|Model
     */
    public function callback(Model $model): array|Collection|Model;
}
