<?php

namespace GuanChanghu\Library\Contracts\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Interface Support
 * @package GuanChanghu\Library\Contracts\Repositories
 */
interface Support
{
    /**
     * @param string $field
     * @param $value
     * @return Support
     */
    public function set(string $field, $value): Support;

    /**
     * @param string $field
     * @param null $default
     * @return mixed
     */
    public function get(string $field, $default = null): mixed;

    /**
     * 获取分页总数
     * @param Builder $builder
     * @return int
     */
    public function total(Builder $builder): int;

    /**
     * @param Attribute $paginateAttribute
     * @return int
     */
    public function offset(Attribute $paginateAttribute): int;

    /**
     * @param Builder $builder
     * @param Attribute $paginateAttribute
     * @return Collection
     */
    public function paginate(Builder $builder, Attribute $paginateAttribute): Collection;
}
