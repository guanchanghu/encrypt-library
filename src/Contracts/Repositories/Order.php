<?php

namespace GuanChanghu\Library\Contracts\Repositories;


/**
 * @author 管昌虎
 * Interface Order
 * @package GuanChanghu\Library\Contracts\Repositories
 * Created on 2023/4/15 09:50
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
interface Order
{
    /**
     * 自定义排序
     * @return Query
     */
    public function orderByCustom(): Query;
}
