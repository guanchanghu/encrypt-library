<?php

namespace GuanChanghu\Library\Contracts\Repositories;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Expression;

/**
 * Interface Paginate
 * @package GuanChanghu\Library\Contracts\Repositories
 */
interface Query
{
    /**
     * @param Attribute $attribute
     */
    public function __construct(Attribute $attribute);

    /**
     * @return Query
     */
    public function initModel(): Query;

    /**
     * @return Query
     */
    public function initBuilder(): Query;

    /**
     * @return Builder
     */
    public function builder(): Builder;

    /**
     * @return Attribute
     */
    public function attribute(): Attribute;

    /**
     * @return Query
     */
    public function before(): Query;

    /**
     * @return Expression|array
     */
    public function select(): Expression|array;

    /**
     * @return Query
     */
    public function query(): Query;

    /**
     * @return Query
     */
    public function render(): Query;
}
