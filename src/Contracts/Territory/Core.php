<?php


namespace GuanChanghu\Library\Contracts\Territory;

use Illuminate\Support\Collection;

/**
 * Interface Facade
 * @package GuanChanghu\Library\Contracts\Territory
 */
interface Core
{
    /**
     * @param Collection|array $collection
     * @param array $fields
     * @return Collection|array
     */
    public function select($collection, array $fields = []);

    /**
     * @param int $id
     * @return array
     */
    public function getTerritoryById(int $id): array;

    /**
     * @param string $shortName
     * @return array
     */
    public function getTerritoryByShortName(string $shortName): array;

    /**
     * @param string $name
     * @return array
     */
    public function getTerritoryByName(string $name): array;

    /**
     * @param string $name
     * @return array
     */
    public function getTerritoryByUniteName(string $name): array;

    /**
     * @return Collection
     */
    public function getTreeTerritory(): Collection;

    /**
     * @param int $parentId
     * @return Collection
     */
    public function getTerritorySubordinateByParentId(int $parentId): Collection;

    /**
     * @param int $id
     * @return Collection
     */
    public function getTreeTerritoryById(int $id): Collection;

    /**
     * @return string
     */
    public function getPrimaryKey(): string;

    /**
     * @param string $shortName
     * @return Collection
     */
    public function getTreeTerritoryByShortName(string $shortName): Collection;

    /**
     * @param string $name
     * @return Collection
     */
    public function getTreeTerritoryByName(string $name): Collection;

    /**
     * @param string $name
     * @return Collection
     */
    public function getTreeTerritoryByUniteName(string $name): Collection;

    /**
     * @return bool
     */
    public function clear(): bool;
}
