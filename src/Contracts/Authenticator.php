<?php
/**
 * 身份验证器
 * Created on 2022/4/15 17:03
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Contracts;

/**
 * Class Authenticator
 * @package GuanChanghu\Library\Contracts
 * Created on 2022/4/15 17:04
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */
interface Authenticator
{
    /**
     * @return string
     */
    public function createSecret(): string;

    /**
     * @param string $label
     * @param string $secret
     * @return string
     */
    public function scan(string $secret = '', string $label = ''): string;

    /**
     * @param string $secret
     * @param string $code
     * @return bool
     */
    public function verify(string $secret, string $code): bool;
}
