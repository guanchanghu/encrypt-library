<?php
/**
 * InitializeSetting
 * Created on 2022/8/26 10:38
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Jobs\Timer;

use GuanChanghu\Library\Facades\Setting;
use Hhxsv5\LaravelS\Swoole\Timer\CronJob;

/**
 * @author 管昌虎
 * Class InitializeSetting
 * @tag encryption free
 * @package GuanChanghu\Library\Jobs\Timer
 * Created on 2022/8/26 10:38
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class InitializeSetting extends CronJob
{
    /**
     * 定时任务的`interval`和`isImmediate`有两种配置方式（二选一）：一是重载对应的方法，二是注册定时任务时传入参数。
     * 重载对应的方法来返回配置：开始
     * @return int
     */
    public function interval()
    {
        return 1000;// 每1秒运行一次
    }

    /**
     * 是否立即执行第一次，false则等待间隔时间后执行第一次
     * @return true
     */
    public function isImmediate()
    {
        return true;
    }

    /**
     * @return void
     */
    public function run()
    {
        Setting::initializeSetting();

        $this->stop();
    }
}
