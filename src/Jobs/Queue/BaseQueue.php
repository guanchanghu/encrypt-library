<?php
/**
 * 基础队列类加密
 * Created on 2022/4/2 9:33
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Jobs\Queue;

use GuanChanghu\Library\Facades\Client;
use GuanChanghu\Library\Facades\Log;
use Throwable;

/**
 * @author 管昌虎
 * Class BaseQueue
 * @tag encryption free
 * @package GuanChanghu\Library\Jobs\Queue
 * Created on 2022/4/2 9:33
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class BaseQueue
{
    use \GuanChanghu\Traits\SafeOperation\Facade;

    /**
     * @var string
     */
    protected string $uuid = '';

    /**
     * @var string
     */
    protected string $client = '';

    /**
     * Encrypt constructor.
     */
    public function __construct()
    {
        $this->uuid = Client::getUuid();
        $this->client = Client::getClient();
    }

    /**
     * @param Throwable $throwable
     * @return void
     */
    public function throwable(Throwable $throwable): void
    {
        $context = [$throwable->getFile() . ':' . $throwable->getLine(), $throwable->getMessage()];

        foreach ($this->logContext() as $filed => $value) {
            $context[$filed] = $value;
        }

        Log::setClient($this->client)->error('uuid：' . $this->uuid, $context);
    }

    /**
     * @param array $parameters
     */
    protected function setParameter(array $parameters): void
    {
        foreach ($parameters as $field => $value) {
            $this->$field = $value;
        }
    }

    /**
     * @return array
     */
    public function logContext(): array
    {
        return [];
    }
}
