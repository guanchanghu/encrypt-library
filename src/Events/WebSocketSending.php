<?php
/**
 * web socket 发送前
 * Created on 2022/8/26 17:52
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Events;

use GuanChanghu\Library\Facades\Client;
use GuanChanghu\Library\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Class WebSocketSending
 * @tag encryption free
 * @package GuanChanghu\Library\Events
 * Created on 2022/8/26 17:53
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class WebSocketSending
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public string $uuid = '';

    /**
     * WebSocketSending constructor.
     * @param string $client
     * @param User $user
     * @param Collection $form
     */
    public function __construct(public string $client, public User $user, public Collection $form)
    {
        $this->uuid = Client::client($client)->getUuid();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
