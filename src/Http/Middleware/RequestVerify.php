<?php

namespace GuanChanghu\Library\Http\Middleware;

use GuanChanghu\Library\Facades\RSA;
use GuanChanghu\Configs\FieldConfig;
use GuanChanghu\Exceptions\DeveloperException;
use GuanChanghu\Library\Facades\Client;
use Closure;
use Illuminate\Http\Request;

/**
 * Class RequestVerify
 * @tag encryption free
 * @package GuanChanghu\Library\Http\Middleware
 */
class RequestVerify
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $verifyResult = $this->verify($request->all());

        if ($verifyResult === true) {
            return $next($request);
        }

        $this->throwException($verifyResult, $request->has('items') ? 2 : 1);

        return $next($request);
    }

    /**
     * @param array $interfaceRequest
     * @return bool|null
     */
    protected function verify(array $interfaceRequest): bool|null
    {
        $clientRSA = RSA::client('verify_' . Client::getClient());

        if (!isset($interfaceRequest['items'])) {
            foreach (array_diff(array_keys($interfaceRequest), ['module', 'data', 'timestamp', 'sign', 'random']) as $key) {
                unset($interfaceRequest[$key]);
            }
        }

        return $clientRSA->verify($interfaceRequest);
    }

    /**
     * @param bool|null $verifyResult
     * @param int $tier
     * @return void
     */
    protected function throwException(bool|null $verifyResult, int $tier): void
    {
        if (is_null($verifyResult)) {
            if ($tier === 2) {
                abort(response()->json(
                    [
                        [
                            'success' => false,
                            'code' => DeveloperException::CODE,
                            FieldConfig::MESSAGE_FIELD => __('guan-changhu::auth.verify.empty'),
                        ]
                    ]
                ));
            } else {
                abort(response()->json(
                    [
                        'success' => false,
                        'code' => DeveloperException::CODE,
                        FieldConfig::MESSAGE_FIELD => __('guan-changhu::auth.verify.empty'),
                    ]
                ));
            }

        }
        if (!$verifyResult) {
            if ($tier === 2) {
                abort(response()->json(
                    [
                        [
                            'success' => false,
                            'code' => DeveloperException::CODE,
                            FieldConfig::MESSAGE_FIELD => __('guan-changhu::auth.verify.error'),
                        ]
                    ]
                ));
            } else {
                abort(response()->json(
                    [
                        'success' => false,
                        'code' => DeveloperException::CODE,
                        FieldConfig::MESSAGE_FIELD => __('guan-changhu::auth.verify.error'),
                    ]
                ));
            }

        }
    }
}
