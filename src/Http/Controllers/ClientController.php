<?php

namespace GuanChanghu\Library\Http\Controllers;

use GuanChanghu\Library\Facades\Client;
use Illuminate\Http\Request;

/**
 * @author 管昌虎
 * Class ClientController
 * @tag encryption free
 * @package GuanChanghu\Library\Http\Controllers
 * Created on 2022/4/9 16:55
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class ClientController extends Controller
{
    /**
     * @param Request $request
     * @param string $client
     * @return array
     */
    public function index(Request $request, string $client): array
    {
        return Client::setController($this)->run();
    }
}
