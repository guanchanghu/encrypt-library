<?php

namespace GuanChanghu\Library\Http\Controllers;


use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 * @author 管昌虎
 * Class IndexController
 * @tag encryption free
 * @package GuanChanghu\Library\Http\Controllers
 * Created on 2022/4/9 16:59
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class IndexController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('welcome');
    }
}
