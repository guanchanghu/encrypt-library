<?php
/**
 * 用户账户表
 * Created on 2022/4/11 16:13
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;


use GuanChanghu\Exceptions\DeveloperException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Exception;
use Illuminate\Support\Collection;

/**
 * @author 管昌虎
 * Class UserAccount
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/11 16:14
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserAccount extends \GuanChanghu\Models\Model
{
    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'user_id' => 'integer',
        'type' => 'integer',
        'real_name' => 'string',
        'account' => 'string',
        'qr_code' => 'string',
        'mobile' => 'string',
        'bank_subbranch' => 'string',
    ];

    /**
     * 账户类型-银行
     */
    public const TYPE_BANK = 0;

    /**
     * 账户类型-支付宝
     */
    public const TYPE_ALI = 1;

    /**
     * 账户类型-微信
     */
    public const TYPE_WECHAT = 2;

    /**
     * 账户类型-账户微信
     */
    public const TYPE_ACCOUNT_WECHAT = 3;

    /**
     * @return string
     */
    public function getTypeLabelAttribute(): string
    {
        return static::getTypeLabel($this['type']);
    }

    /**
     * @param int $type
     * @return string
     */
    public static function getTypeLabel(int $type): string
    {
        return match ($type) {
            static::TYPE_BANK => '银行卡',
            static::TYPE_ALI => '支付宝',
            static::TYPE_WECHAT => '微信',
            static::TYPE_ACCOUNT_WECHAT => '账户微信',
            default => '未知',
        };
    }


    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(config('guan-changhu.models.user'), 'user_id');
    }

    /**
     * @return string
     */
    public function driver(): string
    {
        return match ($this['type']) {
            static::TYPE_ALI => 'ali',
            static::TYPE_ACCOUNT_WECHAT => 'weChat',
            default => '',
        };
    }

    /**
     * @param int $userId
     * @return Collection
     */
    public static function select(int $userId): Collection
    {
        $items = static::query()->select([static::getPrimaryKey(), 'type', 'account'])->where('user_id', $userId)->get();

        $collect = collect();

        $items->each(function (\GuanChanghu\Models\Model $item) use ($collect) {
            $collect->push([
                'value' => $item->getKey(),
                'type' => $item['type'],
                'label' => $item['type'] === static::TYPE_ACCOUNT_WECHAT ? __('guan-changhu::finance.账户微信') : $item['account'],
            ]);
        });

        return $collect;
    }

    /**
     * @param User $user
     * @param Collection $form
     * @return static
     */
    public static function upsert(User $user, Collection $form): static
    {
        if ($form->has('id') && $form->get('id')) {
            // 修改
            $id = $form->pull('id');
            $form->forget('userId');

            $main = static::getById($id);

            $main->changes($form->all());
        } else {
            // 添加
            $form->put('userId', $user->getKey());
            $main = static::initialize($form->all())->changes();
        }

        return $main;
    }
}
