<?php
/**
 * 用户消息表
 * Created on 2022/3/30 17:11
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;


use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @author 管昌虎
 * Class UserMessage
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/3/30 17:13
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserMessage extends \GuanChanghu\Models\Model
{
    /**
     * @var string
     */
    public static string $nonexistent = '没发现此用户消息';

    /**
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'message_content_id' => 'integer',
        'read_num' => 'integer',
        'jump_link_count' => 'integer',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(config('guan-changhu.models.user'), 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function message(): BelongsTo
    {
        return $this->belongsTo(config('guan-changhu.models.message_content'), 'message_content_id');
    }
}
