<?php

namespace GuanChanghu\Library\Models;


use GuanChanghu\Library\Facades\Wallet;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * @author 管昌虎
 * Class UserWalletLog
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/6 11:38
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserWalletLog extends \GuanChanghu\Models\Model
{
    /**
     * @var string
     */
    public static string $nonexistent = '用户钱包日志不存在';

    /**
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'wallet' => 'integer',
        'remark' => 'string',
        'finance_type' => 'integer',
        'after_money' => 'float',
        'money' => 'float',
        'before_money' => 'float',
        'symbol' => 'integer',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(config('guan-changhu.models.user'), 'user_id');
    }

    /**
     * @return string
     */
    public function getWalletLabelAttribute(): string
    {
        return Wallet::getWalletName($this['wallet']);
    }
}
