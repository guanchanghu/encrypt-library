<?php
/**
 * 邮件发送统计日志
 * Created on 2022/4/25 14:57
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;

/**
 * @author 管昌虎
 * Class EmailSendStatisticsLog
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/25 14:57
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class EmailSendStatisticsLog extends \GuanChanghu\Models\Model
{
    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'email' => 'string',
        'date' => 'date',
        'num' => 'int',
    ];
}
