<?php

namespace GuanChanghu\Library\Models;

use GuanChanghu\Library\Facades\Wallet;


/**
 * @author 管昌虎
 * Class UserWallet
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/6 11:38
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserWallet extends \GuanChanghu\Models\Model
{
    /**
     * @var string
     */
    public static string $nonexistent = '用户钱包不存在';

    /**
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'wallet' => 'integer',
        'money' => 'float',
        'grand_total' => 'float',
    ];

    /**
     * @param int $userId
     * @param int $wallet
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|null
     */
    public static function getUserWallet(int $userId, int $wallet): \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|null
    {
        return static::query()
            ->where('user_id', $userId)
            ->where('wallet', $wallet)
            ->select()
            ->first();
    }

    /**
     * @return string
     */
    public function getWalletLabelAttribute(): string
    {
        return Wallet::getWalletName($this['wallet']);
    }
}
