<?php
/**
 * 用户第三方表
 * Created on 2022/4/11 16:15
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @author 管昌虎
 * Class UserSocialite
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/11 16:16
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserSocialite extends \GuanChanghu\Models\Model
{
    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'user_id' => 'integer',
        'provider' => 'string',
        'provider_id' => 'string',
        'provider_second_id' => 'string',
        'gender' => 'string',
        'province' => 'string',
        'city' => 'string',
        'country' => 'string',
        'nickname' => 'string',
        'email' => 'string',
        'avatar' => 'string',
        'remember_token' => 'string',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'provider_id',
        'remember_token',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(config('guan-changhu.models.user'), 'user_id');
    }
}
