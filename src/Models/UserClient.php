<?php

namespace GuanChanghu\Library\Models;

use GuanChanghu\Library\Facades\Client;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @author 管昌虎
 * Class UserClient
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/2/28 8:44
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserClient extends \GuanChanghu\Models\Model
{
    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'user_id' => 'integer',
        'client' => 'string',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(config('guan-changhu.models.user'), 'user_id');
    }

    /**
     * @param int $userId
     * @param string $client
     * @return bool
     */
    public static function userClientExist(int $userId, string $client): bool
    {
        return static::query()->where('user_id', $userId)->where('client', $client)->exists();
    }

    /**
     * @param int $userId
     * @param string $client
     * @return int
     */
    public static function userClientId(int $userId, string $client): int
    {
        return (int)static::query()->where('user_id', $userId)->where('client', $client)->first()?->getKey();
    }

    /**
     * @return array
     */
    public static function getClientMany(): array
    {
        $dest = [];
        foreach (Client::getAllClient() as $client) {
            $dest[] = [
                'value' => $client,
                'label' => $client,
            ];
        }

        return $dest;
    }
}
