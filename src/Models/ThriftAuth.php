<?php
/**
 * thrift auth
 * Created on 2022/10/17 14:03
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;


/**
 * @author 管昌虎
 * Class ThriftAuth
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/10/17 14:03
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class ThriftAuth extends \GuanChanghu\Models\Model
{
    use \GuanChanghu\Traits\GenerateCode\Facade, \GuanChanghu\Traits\Cache\Facade;

    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'code' => 'string',
        'mac' => 'string',
        'ip' => 'string',
    ];

    /**
     * @param string $code
     * @return bool
     */
    public static function checkoutAuth(string $code): bool
    {
        return static::tags()->rememberForever('thrift:auth:' . $code, function () use ($code) {
            return static::query()->where('code', $code)->exists();
        });
    }
}
