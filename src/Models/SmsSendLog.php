<?php
/**
 * sms发送日志表
 * Created on 2022/4/28 15:27
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;


/**
 * @author 管昌虎
 * Class SmsSendLog
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/28 15:27
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class SmsSendLog extends \GuanChanghu\Models\Model
{
    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'driver' => 'string',
        'mobile' => 'string',
        'key' => 'string',
        'template' => 'string',
        'content' => 'string',
        'data' => 'array',
    ];
}
