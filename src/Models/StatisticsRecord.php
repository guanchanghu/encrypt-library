<?php
/**
 * 链接日志
 * Created on 2022/3/3 14:12
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;


/**
 * @author 管昌虎
 * Class StatisticsRecord
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/3/30 17:14
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class StatisticsRecord extends \GuanChanghu\Models\Model
{
    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'table' => 'string',
        'type' => 'string',
        'link' => 'string',
        'year' => 'integer',
        'month' => 'string',
        'day' => 'string',
        'hour' => 'string',
        'value' => 'float',
    ];
}
