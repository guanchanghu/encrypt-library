<?php

namespace GuanChanghu\Library\Models;


use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @author 管昌虎
 * Class MessageContent
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/3/30 16:24
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class MessageContent extends \GuanChanghu\Models\Model
{
    /**
     * @var string
     */
    protected $table = 'message_content';


    /**
     * @var array
     */
    public array $htmlFields = ['content'];

    /**
     * @var string
     */
    public static string $nonexistent = '没发现此消息';

    /**
     * 类型-单发
     */
    public const TYPE_ONE = 0;

    /**
     * 类型-指定用户(多个用户)
     */
    public const TYPE_ASSIGN = 1;

    /**
     * 类型-全部
     */
    public const TYPE_ALL = 2;

    /**
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'type' => 'integer',
        'clients' => 'integer',
        'title' => 'string',
        'icon' => 'string',
        'link' => 'string',
        'send_user_num' => 'integer',
        'read_user_num' => 'integer',
        'read_num' => 'integer',
        'jump_link_count' => 'integer',
        'jump_link_user_num' => 'integer',
        'abstract' => 'string',
        'content' => 'string',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(config('guan-changhu.models.user'), 'user_id');
    }

    /**
     * @return HasOne
     */
    public function userMessage(): HasOne
    {
        return $this->hasOne(config('guan-changhu.models.user_message'), 'message_content_id');
    }

    /**
     * @return HasMany
     */
    public function userMessages(): HasMany
    {
        return $this->hasMany(config('guan-changhu.models.user_message'), 'message_content_id');
    }

    /**
     * @return array
     */
    public static function clientMap(): array
    {
        return config('guan-changhu.clients.and');
    }

    /**
     * @return array
     */
    public function getClientLabelManyAttribute(): array
    {
        $array = [];

        foreach (static::clientMap() as $label => $int) {
            if ($this['clients'] & $int) {
                $array[] = $label;
            }
        }

        return $array;
    }

    /**
     * @param array $excludeClients
     * @return int
     */
    public static function excludeClient(array $excludeClients): int
    {
        $clientMap = static::clientMap();
        $clients = array_diff(array_keys($clientMap), $excludeClients);
        $client = 0;
        foreach ($clients as $item) {
            $client += $clientMap[$item];
        }

        return $client;
    }

    /**
     * @param array $clients
     * @return int
     */
    public static function clients(array $clients = []): int
    {
        $clientMap = static::clientMap();

        if (count($clients) === 0) {
            return array_sum($clientMap);
        }
        $int = 0;
        foreach ($clients as $item) {
            $int += $clientMap[$item];
        }

        return $int;
    }

    /**
     * @return void
     */
    public function read(): void
    {
        if ($this['userMessage']) {
            if ($this['userMessage']['readNum'] === 0) {
                $this->increment('read_user_num');
            }

            $this['userMessage']->increment('read_num');
        }

        $this->increment('read_num');
    }
}
