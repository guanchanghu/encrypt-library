<?php
/**
 * 支付日志表
 * Created on 2022/3/18 10:08
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Library\Models;

use GuanChanghu\Configs\FieldConfig;
use Exception;
use function now;

/**
 * @author 管昌虎
 * Class PayLog
 * @tag encryption free
 * @package GuanChanghu\Library\Models
 * Created on 2022/4/6 11:38
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class PayLog extends \GuanChanghu\Models\Model
{
    /**
     * 数值精度处理
     *
     * @var string[]
     */
    protected $casts = [
        'user_id' => 'integer',
        'order_no' => 'string',
        'amount' => 'float',
        'pay_driver' => 'string',
        'pay_way' => 'string',
        'remark' => 'string',
        'status' => 'integer',
        'paid_at' => 'datetime',
    ];

    /**
     * @return void
     * @throws Exception
     */
    public function success(): void
    {
        $this->changes([
            'status' => FieldConfig::STATUS_SUCCESS,
            'paidAt' => now(),
        ]);
    }
}
