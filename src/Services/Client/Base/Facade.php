<?php


namespace GuanChanghu\Library\Services\Client\Base;


use GuanChanghu\Library\Contracts\Client\Module;
use GuanChanghu\Library\Contracts\Repositories\Attribute as PaginateAttribute;
use Illuminate\Routing\Controller;
use GuanChanghu\Library\Models\User;
use Illuminate\Support\Collection;

/**
 * Class Facade
 * @method __construct(string $client, Controller|null $controller)
 * @method array interfaceCode()                                                                      本版本支持的interface
 * @method string moduleName()                                                                        模块名
 * @method Collection interfaceList()                                                                 接口列表
 * @method Collection anonymityList()                                                                 不需要登录的interface
 * @method Collection transactionList()                                                               需要事务的interface
 * @method User user()                                                                                当前用户,如果没有登录就抛出异常
 * @method void setUser(User|null $user)                                                              设置用户
 * @method User|null lazyUser()                                                                       当前用户,如果没有登录不抛出异常
 * @method bool isLogin()                                                                             是否登录
 * @method int userId()                                                                               当前用户ID
 * @method void interfaceBefore(Collection $form, PaginateAttribute $paginateAttribute)               访问接口之前
 * @method void interfaceAfter(Collection $form, PaginateAttribute $paginateAttribute)                访问接口之后
 * @method string client()                                                                            当前客户端
 * @method bool isAllow(int $interface, array $moduleSwitch)                                          是否允许访问
 * @method string getPermissionPath(int $interface)                                                   权限/前面添加了客户端名
 * @method string getPermissionLabel(int $interface)                                                  权限
 * @method string getModuleNameEnglish()                                                              模块名
 * @method string getModuleMethod(int $interface)                                                     方法
 * @method bool isTransaction(int $interface)                                                         是否需要事务
 * @method string getModuleMethodPermission(int $interface)                                           权限
 * @method bool switch(array $moduleSwitch)                                                           模块开关状态
 * @package GuanChanghu\Library\Services\Client
 */
class Facade extends Encrypt implements Module
{
    /**
     * 模块名
     * @var string
     */
    public string $moduleName;

    /**
     * 接口编号
     * @var array
     */
    public static array $interfaceCode;

    /**
     * @var User|null
     */
    protected User|null $user;

    /**
     * @var string
     */
    protected string $client;

    /**
     * @var Controller|null
     */
    protected Controller|null $controller;
}
