<?php


namespace GuanChanghu\Library\Services\SwooleTable;

use GuanChanghu\Library\Contracts\SwooleTable as SwooleTableContract;
use Swoole\Table;

/**
 * Class Facade
 * @method void initialize(array $otherParam = [])
 * @method string getTableName()
 * @method Table getSwooleTable()
 * @method string getSwooleTableKey(string $identification = '')
 * @method void del(string $identification = '')
 * @method void reset(string $identification = '')
 * @method int|array|string get(string $identification = '', $field = null)
 * @package GuanChanghu\Library\Services\SwooleTable
 */
abstract class Facade extends Encrypt implements SwooleTableContract
{
    /**
     * @var string
     */
    protected string $tableName;

    /**
     * SwooleTable constructor.
     * @param array $otherParam
     */
    abstract public function __construct(array $otherParam = []);

    /**
     * @return array
     */
    abstract public function getSwooleTableField(): array;
}
