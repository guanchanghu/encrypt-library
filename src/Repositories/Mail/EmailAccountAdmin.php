<?php

namespace GuanChanghu\Library\Repositories\Mail;


use GuanChanghu\Library\Contracts\Repositories\Order;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;
use GuanChanghu\Library\Repositories\Base\Facade;

/**
 * @author 管昌虎
 * Class EmailAccountAdmin
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Mail
 * Created on 2023/4/2 11:04
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class EmailAccountAdmin extends Facade implements Order
{
    use \GuanChanghu\Library\Repositories\Base\Traits\Order;

    /**
     * @return Query
     */
    public function initModel(): Query
    {
        $emailAccountModelConfig = config('guan-changhu.models.email_account');

        $this->model = new $emailAccountModelConfig();

        return $this;
    }

    /**
     * @return Query
     */
    public function query(): Query
    {
        $condition = $this->attribute()->condition();
        $builder = $this->builder();

        ModelQuery::queryOptional($builder, $builder->getModel()->getKeyName(), $condition);
        ModelQuery::queryOptional($builder, 'username', $condition);

        $encryption = $condition->get('encryption');
        if ($encryption) {
            if ((int)$condition->get('encryption') === -1) {
                $builder->where('encryption', '');
            } else {
                $builder->where('encryption', $encryption);
            }
        }

        return parent::query();
    }
}
