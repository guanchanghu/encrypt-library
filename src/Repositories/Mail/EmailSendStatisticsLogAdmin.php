<?php

namespace GuanChanghu\Library\Repositories\Mail;

use GuanChanghu\Library\Contracts\Repositories\Order;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;
use GuanChanghu\Library\Repositories\Base\Facade;

/**
 * @author 管昌虎
 * Class EmailSendStatisticsLogAdmin
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Mail
 * Created on 2023/4/2 11:37
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class EmailSendStatisticsLogAdmin extends Facade implements Order
{
    use \GuanChanghu\Library\Repositories\Base\Traits\Order;

    /**
     * @return Query
     */
    public function initModel(): Query
    {
        $modelConfig = config('guan-changhu.models.email_send_statistics_log');

        $this->model = new $modelConfig();

        return $this;
    }

    /**
     * @return Query
     */
    public function query(): Query
    {
        $condition = $this->attribute()->condition();
        $builder = $this->builder();

        ModelQuery::queryOptional($builder, $builder->getModel()->getKeyName(), $condition);
        ModelQuery::queryOptional($builder, 'email', $condition);
        ModelQuery::queryRange($builder, "date", $condition);

        return parent::query();
    }
}
