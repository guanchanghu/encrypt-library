<?php

namespace GuanChanghu\Library\Repositories\Mail;


use GuanChanghu\Library\Contracts\Repositories\Order;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;
use GuanChanghu\Library\Repositories\Base\Facade;
use Illuminate\Database\Eloquent\Model as ModelAlias;
use Illuminate\Support\Str;

/**
 * @author 管昌虎
 * Class EmailSendLogAdmin
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Mail
 * Created on 2023/4/2 11:13
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class EmailSendLogAdmin extends Facade implements Order
{
    use \GuanChanghu\Library\Repositories\Base\Traits\Order;

    /**
     * @return Query
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function initModel(): Query
    {
        $modelConfig = config('guan-changhu.models.email_send_log');

        $this->model = new $modelConfig();

        return $this;
    }

    /**
     * @return Query
     */
    public function query(): Query
    {
        $condition = $this->attribute()->condition();
        $builder = $this->builder();

        ModelQuery::queryOptional($builder, $builder->getModel()->getKeyName(), $condition);
        ModelQuery::queryOptional($builder, 'email', $condition);
        ModelQuery::queryOptional($builder, 'fromEmail', $condition);
        ModelQuery::queryOptional($builder, 'title', $condition);

        ModelQuery::queryRange($builder, Str::camel(ModelAlias::CREATED_AT), $condition);

        return parent::query();
    }
}
