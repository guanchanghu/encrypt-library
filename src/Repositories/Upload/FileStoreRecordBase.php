<?php

namespace GuanChanghu\Library\Repositories\Upload;


use GuanChanghu\Library\Contracts\Repositories\Order;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;
use GuanChanghu\Library\Models\FileStoreRecord as Model;
use GuanChanghu\Library\Repositories\Base\Facade;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model as ModelAlias;

/**
 * @author 管昌虎
 * Class FileStoreRecordBase
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Upload
 * Created on 2023/4/2 10:29
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class FileStoreRecordBase extends Facade implements Order
{
    use \GuanChanghu\Library\Repositories\Base\Traits\Order;

    /**
     * @return Query
     */
    public function initModel(): Query
    {
        $modelConfig = config('guan-changhu.models.file_store_record');

        $this->model = new $modelConfig();

        return $this;
    }

    /**
     * @return Query
     */
    public function query(): Query
    {
        $attribute = $this->attribute();
        $builder = $this->builder();

        // 排序条件
        $condition = $attribute->condition();

        ModelQuery::queryCongruence($builder, 'userId', $condition);

        if ($condition->has('module') && $condition->get('module')) {
            $module = explode('.', $condition->get('module'));


            $builder->where('client', $module[0]);
            if (isset($module[1])) {
                $builder->where('module', $module[1]);
            }
        }

        ModelQuery::queryTimeRange($builder, Str::camel(ModelAlias::CREATED_AT), $condition);

        ModelQuery::queryCongruence($builder, 'type', $condition);

        $builder->where('status', Model::STATUS_SHOW);

        return parent::query();
    }
}
