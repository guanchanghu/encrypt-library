<?php

namespace GuanChanghu\Library\Repositories\Wallet;


/**
 * @author 管昌虎
 * Class UserWalletLogNotExistsUser
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Wallet
 * Created on 2023/4/2 12:45
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserWalletLogNotExistsUser extends UserWalletLogBase
{
}
