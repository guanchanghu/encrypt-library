<?php

namespace GuanChanghu\Library\Repositories\Wallet;


use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;

/**
 * @author 管昌虎
 * Class UserWalletLogExistsUser
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Wallet
 * Created on 2023/4/2 12:45
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserWalletLogExistsUser extends UserWalletLogBase
{
    /**
     * @return Query
     */
    public function query(): Query
    {
        $builder = $this->builder();

        ModelQuery::queryWithUser($builder);

        return parent::query();
    }
}
