<?php

namespace GuanChanghu\Library\Repositories\Wallet;


use GuanChanghu\Library\Contracts\Repositories\Order;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;
use GuanChanghu\Library\Repositories\Base\Facade;

/**
 * @author 管昌虎
 * Class UserWalletLogBase
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Wallet
 * Created on 2023/4/2 12:45
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class UserWalletLogBase extends Facade implements Order
{
    use \GuanChanghu\Library\Repositories\Base\Traits\Order;

    /**
     * @return Query
     */
    public function initModel(): Query
    {
        $modelConfig = config('guan-changhu.models.user_wallet_log');

        $this->model = new $modelConfig();

        return $this;
    }

    /**
     * @return Query
     */
    public function query(): Query
    {
        $builder = $this->builder();
        $condition = $this->attribute()->condition();

        ModelQuery::queryOptional($builder, $builder->getModel()->getKeyName(), $condition);
        ModelQuery::queryCongruence($builder, 'wallet', $condition);
        ModelQuery::queryCongruence($builder, "financeType", $condition);
        ModelQuery::queryCongruence($builder, "symbol", $condition);
        ModelQuery::queryCongruence($builder, 'userId', $condition);
        ModelQuery::queryUserInfo($builder, 'user', $condition);

        return parent::query();
    }
}
