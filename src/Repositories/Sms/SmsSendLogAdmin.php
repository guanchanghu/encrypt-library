<?php

namespace GuanChanghu\Library\Repositories\Sms;


use GuanChanghu\Library\Contracts\Repositories\Order;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;
use GuanChanghu\Library\Repositories\Base\Facade;
use Illuminate\Database\Eloquent\Model as ModelAlias;
use Illuminate\Support\Str;

/**
 * @author 管昌虎
 * Class SmsSendLogAdmin
 * @tag encryption free
 * @package GuanChanghu\Library\Services\Sms\Paginate
 * Created on 2023/4/2 12:33
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class SmsSendLogAdmin extends Facade implements Order
{
    use \GuanChanghu\Library\Repositories\Base\Traits\Order;

    /**
     * @return Query
     */
    public function initModel(): Query
    {
        $modelConfig = config('guan-changhu.models.sms_send_log');

        $this->model = new $modelConfig();

        return $this;
    }

    /**
     * @return Query
     */
    public function query(): Query
    {
        $builder = $this->builder();
        $condition = $this->attribute()->condition();

        ModelQuery::queryOptional($builder, $builder->getModel()->getKeyName(), $condition);
        ModelQuery::queryOptional($builder, 'mobile', $condition);
        ModelQuery::queryCongruence($builder, 'key', $condition);
        ModelQuery::queryCongruence($builder, 'template', $condition);

        ModelQuery::queryRange($builder, Str::camel(ModelAlias::CREATED_AT), $condition);

        return parent::query();
    }
}
