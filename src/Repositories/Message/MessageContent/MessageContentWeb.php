<?php

namespace GuanChanghu\Library\Repositories\Message\MessageContent;

use Illuminate\Database\Eloquent\Model as ModelAlias;
use Illuminate\Database\Query\Expression;

/**
 * @author 管昌虎
 * Class MessageContentWeb
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Message\MessageContent
 * Created on 2023/4/2 11:43
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class MessageContentWeb extends MessageContentBase
{
    /**
     * @return array|Expression
     */
    public function select(): array|Expression
    {
        return [
            $this->builder()->getModel()->getKeyName(),
            'title', 'icon', 'link', 'abstract',
            'content', ModelAlias::CREATED_AT,
        ];
    }
}
