<?php

namespace GuanChanghu\Library\Repositories\Message\MessageContent;


use Illuminate\Database\Eloquent\Model as ModelAlias;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Library\Facades\ModelQuery;
use Illuminate\Database\Query\Expression;

/**
 * @author 管昌虎
 * Class MessageContentAdmin
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Message\MessageContent
 * Created on 2023/4/2 11:43
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class MessageContentAdmin extends MessageContentBase
{
    /**
     * @return array|Expression
     */
    public function select(): array|Expression
    {
        return [
            $this->builder()->getModel()->getKeyName(),
            'user_id', 'clients', 'type', 'title', 'icon',
            'link', 'send_user_num', 'read_user_num',
            'read_num', 'jump_link_count',
            'jump_link_user_num', ModelAlias::CREATED_AT
        ];
    }

    /**
     * @return Query
     */
    public function query(): Query
    {
        $builder = $this->builder();

        ModelQuery::queryWithUser($builder);

        return parent::query();
    }
}
