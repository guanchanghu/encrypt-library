<?php

namespace GuanChanghu\Library\Repositories\Base\Traits;

use GuanChanghu\Library\Contracts\Repositories\Query;
use Illuminate\Support\Str;

/**
 * @author 管昌虎
 * Trait Order
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Base\Traits
 * Created on 2023/4/15 21:20
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Order
{
    /**
     * 自定义排序
     * @return Query
     */
    public function orderByCustom(): Query
    {
        $sorter = $this->attribute()->sorter();
        $builder = $this->builder();

        if ($sorter->count()) {
            $sorter->each(function ($direction, $field) use ($builder) {
                $builder->orderBy(Str::snake($field), $direction);
            });
        }

        return $this;
    }
}
