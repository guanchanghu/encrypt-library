<?php

namespace GuanChanghu\Library\Repositories\Base\Traits;

use GuanChanghu\Library\Contracts\Repositories\Query;

/**
 * @author 管昌虎
 * Trait Builder
 * @tag encryption free
 * @package GuanChanghu\Library\Repositories\Base\Traits
 * Created on 2023/4/15 21:20
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
trait Builder
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected \Illuminate\Database\Eloquent\Builder $builder;

    /**
     * @return Query
     */
    public function initBuilder(): Query
    {
        $this->builder = $this->model->newQuery();

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function builder(): \Illuminate\Database\Eloquent\Builder
    {
        return $this->builder;
    }
}
