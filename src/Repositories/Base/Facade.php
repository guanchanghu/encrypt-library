<?php

namespace GuanChanghu\Library\Repositories\Base;


use GuanChanghu\Library\Contracts\Repositories\Attribute;
use GuanChanghu\Library\Contracts\Repositories\Query;
use GuanChanghu\Models\Model;
use Illuminate\Database\Query\Expression;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @author 管昌虎
 * Class Facade
 * @method void __construct(Attribute $attribute)
 * @method static static make(...$parameter)
 * @method Query initBuilder()
 * @method Query initModel()
 * @method Attribute attribute()
 * @method \Illuminate\Database\Eloquent\Builder builder()
 * @method Expression|array select()
 * @method Query query()
 * @method Query before()
 * @method Query render()
 * @package GuanChanghu\Library\Repositories\Base
 * Created on 2023/3/25 20:40
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
abstract class Facade extends Encrypt implements Query
{
    /**
     * @var Model|Authenticatable
     */
    protected Model|Authenticatable $model;

    /**
     * @var string
     */
    protected string $modelClass;
}
