<?php

namespace GuanChanghu\Library\Console;


use Illuminate\Console\Command;
use Throwable;

/**
 * Class BaseCommand
 * @tag encryption free
 * @package GuanChanghu\Library\Console
 */
abstract class BaseCommand extends Command
{
    use \GuanChanghu\Traits\SafeOperation\Facade;

    /**
     * @param Throwable $throwable
     * @return void
     */
    public function throwable(Throwable $throwable): void
    {
        $this->error($throwable->getMessage());
        $this->error($throwable->getFile() . ':' . $throwable->getLine());
    }

    /**
     * @param string $string
     * @param null $verbosity
     */
    public function info($string, $verbosity = null)
    {
        PHP_SAPI === 'cli' && parent::info($string, $verbosity);
    }

    /**
     * @param string $string
     * @param null $verbosity
     */
    public function error($string, $verbosity = null)
    {
        PHP_SAPI === 'cli' && parent::error($string, $verbosity);
    }
}
