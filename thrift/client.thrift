namespace php GuanChanghu.Library.Thrift.Client


// 定义用户接口
service Client {
    string run(1:required string code, 2:required string client, 3:required string md, 4:required i32 itfc, 5:required string userId, 6:required i32 version, 7:required string lit, 8:required string form)
}
