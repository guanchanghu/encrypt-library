<?php return array (
  'file_exist' => '已上傳過此檔案！',
  'file_not_exist' => '上傳文件不存在！',
  'file_extension_error' => '上傳檔案格式不對，不是:extension格式檔案！',
  'copy_suffix_null' => '暫時不支持:suffix此尾碼的',
  'original' => 
  array (
    'not_exist' => '請傳遞原檔案資訊',
    'not_name' => '請傳遞原檔名',
    'not_size' => '請傳遞原文件大小',
    'not_type' => '請傳遞原檔案類型',
    'name_error' => '原檔名和你傳遞上來的不一致',
    'size_error' => '原檔名大小和你傳遞上來的不一致',
    'type_error' => '原檔名類型和你傳遞上來的不一致',
  ),
  'forbid' => '禁止上傳此:extension格式的檔案',
);