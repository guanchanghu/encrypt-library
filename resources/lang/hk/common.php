<?php return array (
  'gender' => 
  array (
    '男' => '男',
    '女' => '女',
    '未知' => '未知',
  ),
  'create' => 
  array (
    'success' => '添加成功',
    'error' => '添加失敗',
  ),
  'save' => 
  array (
    'success' => '保存成功',
    'error' => '保存失敗',
  ),
  'change' => 
  array (
    'success' => '修改成功',
    'error' => '修改失敗',
  ),
  'remove' => 
  array (
    'success' => '移除成功',
    'error' => '移除失敗',
  ),
  'operate' => 
  array (
    'success' => '操作成功',
    'error' => '操作失敗',
  ),
  'export' => 
  array (
    'success' => '匯出成功',
    'error' => '匯出失敗',
  ),
  'territory' => 
  array (
    'dataNull' => '地域數據沒查到',
  ),
  'error' => 
  array (
    'developer' => '系統錯誤，請聯系管理員',
  ),
  'lead' => 
  array (
    'success' => '導入成功',
    'error' => '導入失敗',
  ),
  'submit' => 
  array (
    'success' => '提交成功',
    'error' => '提交失敗',
  ),
  'rollback' => 
  array (
    'success' => '撤回成功',
    'error' => '撤回失敗',
  ),
  'pass' => 
  array (
    'success' => '通過成功',
    'error' => '通過失敗',
  ),
  'refuse' => 
  array (
    'success' => '拒絕成功',
    'error' => '拒絕失敗',
  ),
  'not_belong' => '他不屬於你',
  '录入中' => '錄入中',
  '提交审核' => '提交稽核',
  '审核通过' => '稽核通過',
  '审核不通过' => '稽核不通過',
  '暂无此状态' => '暫無此狀態',
  'wallet' => 
  array (
    'unknown' => '暫無此錢包',
    'minus' => '錢包不能為負數',
    'not_exists' => '用戶錢包不存在',
    '余额' => '餘額',
    '大币' => '大幣',
    '小币' => '小幣',
  ),
  'update' => 
  array (
    'success' => '更新成功',
    'error' => '更新失敗',
  ),
  'reset' => 
  array (
    'success' => '重置成功',
    'error' => '重置失敗',
  ),
  'init' => 
  array (
    'success' => '初始化成功',
    'error' => '初始化失敗',
  ),
  'delete' => 
  array (
    'success' => '删除成功',
    'error' => '删除失敗',
  ),
  'set' => 
  array (
    'success' => '設定成功',
    'error' => '設定失敗',
  ),
  'bind' => 
  array (
    'success' => '綁定成功',
    'error' => '綁定失敗',
  ),
  'send' => 
  array (
    'success' => '發送成功',
    'error' => '發送失敗',
  ),
  'config' => 
  array (
    'success' => '配寘成功',
    'error' => '配寘失敗',
  ),
  'record' => 
  array (
    'success' => '記錄成功',
    'error' => '記錄失敗',
  ),
  'feedback' => 
  array (
    'success' => '迴響成功',
    'error' => '迴響失敗',
  ),
  'reply' => 
  array (
    'success' => '回復成功',
    'error' => '回復失敗',
  ),
  'apply' => 
  array (
    'success' => '申請成功',
    'error' => '申請失敗',
  ),
  'pay' => 
  array (
    'success' => '支付成功',
    'error' => '支付失敗',
    'order' => 
    array (
      'none' => '沒有查到此訂單',
      'repetition_paid' => '已支付，不能重複支付',
      'amount_error' => '支付金額錯誤',
    ),
  ),
  'close' => 
  array (
    'success' => '關閉成功',
    'error' => '關閉失敗',
  ),
  'move' => 
  array (
    'success' => '移動成功',
    'error' => '移動失敗',
  ),
  'time' => 
  array (
    'status' => 
    array (
      '未开始' => '未開始',
      '已开始' => '已開始',
      '已结束' => '已結束',
      '时间范围状态不对' => '時間範圍狀態不對',
    ),
    'expire' => '已結束',
  ),
  'like' => 
  array (
    'add' => 
    array (
      'success' => '點贊成功',
      'error' => '點贊失敗',
    ),
    'cancel' => 
    array (
      'success' => '取消點贊成功',
      'error' => '取消點贊失敗',
    ),
  ),
  'trample' => 
  array (
    'add' => 
    array (
      'success' => '踩成功',
      'error' => '踩失敗',
    ),
    'cancel' => 
    array (
      'success' => '取消踩成功',
      'error' => '取消踩失敗',
    ),
  ),
  'collect' => 
  array (
    'add' => 
    array (
      'success' => '收藏成功',
      'error' => '收藏失敗',
    ),
    'cancel' => 
    array (
      'success' => '取消收藏成功',
      'error' => '取消收藏失敗',
    ),
  ),
  'share' => 
  array (
    'success' => '分享成功',
    'error' => '分享失敗',
  ),
  'comment' => 
  array (
    'success' => '評論成功',
    'error' => '評論失敗',
  ),
  'cancel' => 
  array (
    'success' => '取消成功',
    'error' => '取消失敗',
  ),
  'upgrade' => 
  array (
    'success' => '陞級成功',
    'error' => '陞級失敗',
  ),
  'format' => 
  array (
    'item_max_length' => '格式化之後每個大版本中間版本小版本位數不能超過:count比特',
  ),
  'lack_param' => '缺少參數：param',
  'sms' => 
  array (
    'hz_limit' => '短信發送太過頻繁',
  ),
);