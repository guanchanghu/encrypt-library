<?php return array (
  'failed' => '這些憑據與我們的記錄不匹配。',
  'password' => '提供的密碼不正確。',
  'throttle' => '登入嘗試次數過多。 請在:seconds秒後重試。',
  'logout' => 
  array (
    'success' => '退出成功',
    'error' => '退出失敗',
  ),
  'login' => 
  array (
    'error' => 
    array (
      'account' => '帳戶名稱錯誤。 找不到此用戶',
      'no' => '請登入並重試！',
      'client' => '您無法登入到此平臺',
      'password_code' => '登入密碼和二次驗證碼必須填寫一個',
      'password' => '密碼錯誤',
    ),
  ),
  'register' => 
  array (
    'success' => '登入成功',
    'error' => '登入失敗',
    'exists' => '用戶已注册',
    'not_exists' => '用戶未注册',
    'client_exists' => '用戶已注册過，請用原先的密碼登入',
  ),
  'change' => 
  array (
    'success' => '用戶修改成功',
    'error' => '用戶修改成功或失敗',
    'no_difference' => 
    array (
      'email' => '新舊郵箱一致',
      'mobile' => '新舊手機號一致',
    ),
    'exists' => 
    array (
      'email' => '新郵箱已被注册',
      'mobile' => '新手機號已被注册',
    ),
  ),
  'recommend' => 
  array (
    'exists' => '推薦關係已存在',
    'empty' => '推薦人不能為空',
    'error' => '推薦人不存在',
    'user' => 
    array (
      'not_bind' => '推薦人沒有具有約束力的推薦關係',
    ),
    'invite_code' => 
    array (
      'error' => '邀請程式碼錯誤，找不到任何人',
    ),
  ),
  'verify' => 
  array (
    'empty' => '請傳遞簽名',
    'error' => '伺服器端簽名驗證錯誤',
    'way_empty' => '驗證錯誤',
    'safe_password' => '安全密碼錯誤',
  ),
  'status' => 
  array (
    'freeze' => '你的帳戶已被凍結',
    'block' => '你的帳戶已被拉黑',
    'error' => '帳戶狀態錯誤',
    'freeze_time_error' => '凍結時間不能小於當前時間',
  ),
  'exists' => '用戶已存在',
  'no_exists' => '用戶不存在',
  'expire' => '登入超時，請重新登入',
  'socialite' => 
  array (
    'user_error' => '解析出來的用戶和傳遞的用戶不一致',
  ),
  'account' => 
  array (
    'type_error' => '帳戶類型錯誤',
  ),
  'authenticator_secret' => 
  array (
    'not_bind' => '沒有綁定二次驗證',
    'secret_exists_not_code' => '你已綁定二次驗證，請驗證原先秘鑰',
    'verify_error' => '驗證錯誤',
    'verify_error_old' => '舊秘鑰，驗證錯誤',
    'verify_error_new' => '新秘鑰，驗證錯誤',
    'verify_error_password' => '密碼驗證錯誤',
  ),
  'captcha' => 
  array (
    'img' => '圖片驗證碼錯誤',
    'email' => '郵箱驗證碼錯誤',
    'mobile' => '簡訊驗證碼錯誤',
  ),
  'bind' => 
  array (
    'exists' => 
    array (
      'mobile' => '手機號已綁定，不能重複綁定',
      'email' => '郵箱已綁定，不能重複綁定',
    ),
  ),
  'init' => 
  array (
    'exists' => 
    array (
      'safe_password' => '安全密碼已經初始化，不能重複初始化',
    ),
  ),
  'no_bind' => 
  array (
    'mobile' => '未綁定手機號',
    'email' => '未綁定郵箱',
  ),
  'authentication' => 
  array (
    'accomplish' => 
    array (
      'bind_real_name_error' => '已實名認證不能更換姓名',
    ),
    'certified' => 
    array (
      'bind_real_name_error' => '已實名認證不能更換姓名',
    ),
    'exists' => '已認證，請不要重複認證',
    'not_token' => '請刷臉認證',
    'not_pay' => '實名認證需要付款，請先付款',
    'not_log_info' => '沒有發現此認證資訊',
    'error' => '認證不通過',
    'success' => '認證成功',
    'success_hint_pay' => '識別成功，請及時付款',
    'not_need_pay' => '認證不需要付款，如遇到困難請聯系管理員',
    'after_need_authentication' => '請先認證再付款',
    'verify_before_payment' => '請先認證在付款',
    'id_number_pass' => '此認證的身份證號已被認證通過',
    'id_number_length_error' => '身份證號長度不合法',
    'id_number_check_code_error' => '身份證號檢驗碼不合法',
    'id_number_check_code_bit_error' => '身份證號檢驗碼錯誤',
    'id_number_birthday_error' => '身份證號出生年月不合法',
    'id_number_territory_error' => '身份證號地域位置不合法',
    'artificial_auth_wait' => '提交成功，請等待稽核結果',
  ),
);