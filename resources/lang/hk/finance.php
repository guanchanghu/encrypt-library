<?php return array (
  'error' => 
  array (
    'unknown' => '無此訂單',
    'pay_driver' => '訂單通路不對',
    'amount' => '訂單數值不對',
  ),
  'recharge' => 
  array (
    'error' => 
    array (
      'min' => '充值金額不能小於：min',
      'max' => '充值金額不能大於：max',
      'multiple' => '充值金額必須是:multiple的整數倍',
      'repetition' => '訂單不能重複使用',
    ),
    'success' => '充值成功',
  ),
  'transfer' => 
  array (
    'success' => '轉帳成功',
    'error' => '轉帳失敗',
    'receipt' => 
    array (
      'success' => '接受成功',
      'error' => '接受失敗',
    ),
    'refuse' => 
    array (
      'success' => '拒絕成功',
      'error' => '拒絕失敗',
    ),
  ),
  'convert' => 
  array (
    'success' => '兌換成功',
    'error' => '兌換失敗',
    'wallet_eq' => '轉換錢包不能選擇一樣的',
    'switch' => ':fromWallet不能向:wallet兌換',
  ),
  'withdraw' => 
  array (
    'channel_error' => '暫不提供此驅動管道提現',
    'auto_error' => '自動提現失敗',
  ),
  'wallet' => 
  array (
    'unknown' => '暫無此錢包',
    'minus' => ':walletName不足',
    'not_exists' => '用戶錢包不存在',
    '余额' => '餘額',
    '大币' => '大幣',
    '小币' => '小幣',
    '积分' => '積分',
    'not_open' => '此錢包尚未開啟',
  ),
  'pay_way_error' => '支付方式不對',
  '账户微信' => '帳戶微信',
  'award' => 
  array (
    'register_close' => '注册獎勵已關閉',
  ),
  'pay' => 
  array (
    'driver_close' => '支付方式已關閉，請更換一種試一試',
    'wallet_close' => '該種錢包支付方式已關閉，請更換一種試一試',
    'success' => '支付成功',
    'error' => '支付失敗',
  ),
  'red_package' => 
  array (
    'receive' => '紅包已被領取完畢',
    'expire' => '紅包已過期',
    'error' => '領取紅包失敗',
    'draw_exists' => '紅包不能重複領取',
    'receiver_error' => '這個紅包不是發送給你的，你不能領取',
    'receiver_empty_error' => '請指定接收人',
    'friend_group_empty_error' => '請指定發送的群組',
    'status_error' => '紅包狀態錯誤',
    'draw_friend' => '只能是好友才能領取此紅包',
    'friend_group_error' => '好友群組錯誤',
    'friend_group_send_error' => '你不在此群中，不能發送紅包',
    'close' => '發紅包功能已關閉',
    'wallet_close' => '此錢包發紅包功能已關閉',
    'min_error' => '發的紅包太小了，必須大於：min',
    'max_error' => '發的紅包太大了，必須小於：max',
    'max_num_error' => '發的紅包數量過多，必須小於：max',
    'money_too_small' => '您發的紅包總金額太小了，不够人手一個的',
  ),
);