<?php

return [
    'file_exist' => '已上传过此文件！',
    'file_not_exist' => '上传文件不存在！',
    'file_extension_error' => '上传文件格式不对，不是 :extension 格式文件！',
    'copy_suffix_null' => '暂时不支持 :suffix 此后缀的',
    'forbid' => '禁止上传此 :extension 格式的文件',
    'original' => [
        'not_exist' => '请传递原文件信息',
        'not_name' => '请传递原文件名',
        'not_size' => '请传递原文件大小',
        'not_type' => '请传递原文件类型',
        'name_error' => '原文件名和你传递上来的不一致',
        'size_error' => '原文件名大小和你传递上来的不一致',
        'type_error' => '原文件名类型和你传递上来的不一致',
    ],
];
