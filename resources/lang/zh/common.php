<?php

return [
    'gender' => [
        '男' => '男',
        '女' => '女',
        '未知' => '未知',
    ],
    'create' => [
        'success' => '添加成功',
        'error' => '添加失败',
    ],
    'save' => [
        'success' => '保存成功',
        'error' => '保存失败',
    ],
    'update' => [
        'success' => '更新成功',
        'error' => '更新失败',
    ],
    'change' => [
        'success' => '修改成功',
        'error' => '修改失败',
    ],
    'reset' => [
        'success' => '重置成功',
        'error' => '重置失败',
    ],
    'init' => [
        'success' => '初始化成功',
        'error' => '初始化失败',
    ],
    'delete' => [
        'success' => '删除成功',
        'error' => '删除失败',
    ],
    'remove' => [
        'success' => '移除成功',
        'error' => '移除失败',
    ],
    'set' => [
        'success' => '设置成功',
        'error' => '设置失败',
    ],
    'bind' => [
        'success' => '绑定成功',
        'error' => '绑定失败',
    ],
    'send' => [
        'success' => '发送成功',
        'error' => '发送失败',
    ],
    'config' => [
        'success' => '配置成功',
        'error' => '配置失败',
    ],
    'record' => [
        'success' => '记录成功',
        'error' => '记录失败',
    ],
    'feedback' => [
        'success' => '反馈成功',
        'error' => '反馈失败',
    ],
    'operate' => [
        'success' => '操作成功',
        'error' => '操作失败',
    ],
    'export' => [
        'success' => '导出成功',
        'error' => '导出失败',
    ],
    'territory' => [
        'dataNull' => '地域数据没查到',
    ],
    'error' => [
        'developer' => '系统错误，请联系管理员'
    ],
    'lead' => [
        'success' => '导入成功',
        'error' => '导入失败',
    ],
    'reply' => [
        'success' => '回复成功',
        'error' => '回复失败',
    ],
    'apply' => [
        'success' => '申请成功',
        'error' => '申请失败',
    ],
    'pay' => [
        'success' => '支付成功',
        'error' => '支付失败',
        'order' => [
            'none' => '没有查到此订单',
            'repetition_paid' => '已支付，不能重复支付',
            'amount_error' => '支付金额错误',
        ],
    ],
    'close' => [
        'success' => '关闭成功',
        'error' => '关闭失败',
    ],
    'submit' => [
        'success' => '提交成功',
        'error' => '提交失败',
    ],
    'rollback' => [
        'success' => '撤回成功',
        'error' => '撤回失败',
    ],
    'pass' => [
        'success' => '通过成功',
        'error' => '通过失败',
    ],
    'move' => [
        'success' => '移动成功',
        'error' => '移动失败',
    ],
    'refuse' => [
        'success' => '拒绝成功',
        'error' => '拒绝失败',
    ],
    'not_belong' => '他不属于你',
    '录入中' => '录入中',
    '提交审核' => '提交审核',
    '审核通过' => '审核通过',
    '审核不通过' => '审核不通过',
    '暂无此状态' => '暂无此状态',
    'time' => [
        'status' => [
            '未开始' => '未开始',
            '已开始' => '已开始',
            '已结束' => '已结束',
            '时间范围状态不对' => '时间范围状态不对',
        ],
        'expire' => '已结束',
    ],
    'like' => [
        'add' => [
            'success' => '点赞成功',
            'error' => '点赞失败',
        ],
        'cancel' => [
            'success' => '取消点赞成功',
            'error' => '取消点赞失败',
        ],
    ],
    'trample' => [
        'add' => [
            'success' => '踩成功',
            'error' => '踩失败',
        ],
        'cancel' => [
            'success' => '取消踩成功',
            'error' => '取消踩失败',
        ],
    ],
    'collect' => [
        'add' => [
            'success' => '收藏成功',
            'error' => '收藏失败',
        ],
        'cancel' => [
            'success' => '取消收藏成功',
            'error' => '取消收藏失败',
        ],
    ],
    'share' => [
        'success' => '分享成功',
        'error' => '分享失败',
    ],
    'comment' => [
        'success' => '评论成功',
        'error' => '评论失败',
    ],
    'cancel' => [
        'success' => '取消成功',
        'error' => '取消失败',
    ],
    'upgrade' => [
        'success' => '升级成功',
        'error' => '升级失败',
    ],
    'format' => [
        'item_max_length' => '格式化之后每个大版本中间版本小版本位数不能超过 :count 位',
    ],
    'lack_param' => '缺少参数 :param',
    'sms' => [
        'hz_limit' => '短信发送太过频繁',
    ],
];
