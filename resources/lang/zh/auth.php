<?php return array (
  'failed' => '这些凭据与我们的记录不匹配。',
  'password' => '提供的密码不正确。',
  'throttle' => '登录尝试次数过多。请在:seconds秒后重试。',
  'status' => 
  array (
    'freeze' => '你的账户已被冻结',
    'block' => '你的账户已被拉黑',
    'error' => '账户状态错误',
    'freeze_time_error' => '冻结时间不能小于当前时间',
  ),
  'logout' => 
  array (
    'success' => '退出成功',
    'error' => '退出失败',
  ),
  'login' => 
  array (
    'error' => 
    array (
      'account' => '帐户名称错误。找不到此用户',
      'no' => '请登录并重试！',
      'client' => '您无法登录到此平台',
      'password_code' => '登录密码和二次验证码必须填写一个',
      'password' => '密码错误',
    ),
  ),
  'register' => 
  array (
    'success' => '登录成功',
    'error' => '登录失败',
    'exists' => '用户已注册',
    'not_exists' => '用户未注册',
    'client_exists' => '用户已注册过，请用原先的密码登录',
  ),
  'change' => 
  array (
    'no_difference' => 
    array (
      'email' => '新旧邮箱一致',
      'mobile' => '新旧手机号一致',
    ),
    'exists' => 
    array (
      'email' => '新邮箱已被注册',
      'mobile' => '新手机号已被注册',
    ),
    'success' => '用户修改成功',
    'error' => '用户修改成功或失败',
  ),
  'recommend' => 
  array (
    'exists' => '推荐关系已存在',
    'empty' => '推荐人不能为空',
    'error' => '推荐人不存在',
    'user' => 
    array (
      'not_bind' => '推荐人没有具有约束力的推荐关系',
    ),
    'invite_code' => 
    array (
      'error' => '邀请代码错误，找不到任何人',
    ),
  ),
  'verify' => 
  array (
    'empty' => '请传递签名',
    'error' => '服务器端签名验证错误',
    'way_empty' => '验证错误',
    'safe_password' => '安全密码错误',
  ),
  'exists' => '用户已存在',
  'no_exists' => '用户不存在',
  'expire' => '登录超时，请重新登录',
  'socialite' => 
  array (
    'user_error' => '解析出来的用户和传递的用户不一致',
  ),
  'account' => 
  array (
    'type_error' => '账户类型错误',
  ),
  'authenticator_secret' => 
  array (
    'not_bind' => '没有绑定二次验证',
    'secret_exists_not_code' => '你已绑定二次验证，请验证原先秘钥',
    'verify_error' => '验证错误',
    'verify_error_old' => '旧秘钥，验证错误',
    'verify_error_new' => '新秘钥，验证错误',
    'verify_error_password' => '密码验证错误',
  ),
  'captcha' => 
  array (
    'img' => '图片验证码错误',
    'email' => '邮箱验证码错误',
    'mobile' => '短信验证码错误',
  ),
  'bind' => 
  array (
    'exists' => 
    array (
      'mobile' => '手机号已绑定，不能重复绑定',
      'email' => '邮箱已绑定，不能重复绑定',
    ),
  ),
  'init' => 
  array (
    'exists' => 
    array (
      'safe_password' => '安全密码已经初始化，不能重复初始化',
    ),
  ),
  'no_bind' => 
  array (
    'mobile' => '未绑定手机号',
    'email' => '未绑定邮箱',
  ),
  'authentication' => 
  array (
    'accomplish' => 
    array (
      'bind_real_name_error' => '已实名认证不能更换姓名',
    ),
    'certified' => 
    array (
      'bind_real_name_error' => '已实名认证不能更换姓名',
    ),
    'exists' => '已认证，请不要重复认证',
    'not_token' => '请刷脸认证',
    'not_pay' => '实名认证需要付款，请先付款',
    'not_log_info' => '没有发现此认证信息',
    'error' => '认证不通过',
    'success' => '认证成功',
    'success_hint_pay' => '识别成功，请及时付款',
    'not_need_pay' => '认证不需要付款，如遇到困难请联系管理员',
    'after_need_authentication' => '请先认证再付款',
    'verify_before_payment' => '请先认证在付款',
    'id_number_pass' => '此认证的身份证号已被认证通过',
    'id_number_length_error' => '身份证号长度不合法',
    'id_number_check_code_error' => '身份证号检验码不合法',
    'id_number_check_code_bit_error' => '身份证号检验码错误',
    'id_number_birthday_error' => '身份证号出生年月不合法',
    'id_number_territory_error' => '身份证号地域位置不合法',
    'artificial_auth_wait' => '提交成功，请等待审核结果',
  ),
);