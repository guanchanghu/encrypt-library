<?php return array (
  'failed' => 'These credentials do not match our records.',
  'password' => 'The password provided is incorrect.',
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
  'logout' => 
  array (
    'success' => 'Exit successfully',
    'error' => 'Exit failed',
  ),
  'login' => 
  array (
    'error' => 
    array (
      'account' => 'Account name error. Unable to find this user',
      'no' => 'Please log in and try again!',
      'client' => 'You cannot log in to this platform',
      'password_code' => 'Login password and secondary verification code must be filled in one',
      'password' => 'Password error',
    ),
  ),
  'register' => 
  array (
    'success' => 'Login successful',
    'error' => 'Login failed',
    'exists' => 'User registered',
    'not_exists' => 'User not registered',
    'client_exists' => 'The user has already registered, please log in with the original password',
  ),
  'change' => 
  array (
    'success' => 'User modification successful',
    'error' => 'User modification successful or failed',
    'no_difference' => 
    array (
      'email' => 'Consistent new and old email addresses',
      'mobile' => 'Consistent new and old phone numbers',
    ),
    'exists' => 
    array (
      'email' => 'The new email has been registered',
      'mobile' => 'The new phone number has been registered',
    ),
  ),
  'recommend' => 
  array (
    'exists' => 'Recommendation relationship already exists',
    'empty' => 'The recommender cannot be empty',
    'error' => 'The recommender does not exist',
    'user' => 
    array (
      'not_bind' => 'The recommender does not have a binding recommendation relationship',
    ),
    'invite_code' => 
    array (
      'error' => 'Invitation code error, no one found',
    ),
  ),
  'verify' => 
  array (
    'empty' => 'Please pass on your signature',
    'error' => 'Server side signature verification error',
    'way_empty' => 'Validation error',
    'safe_password' => 'Security password error',
  ),
  'status' => 
  array (
    'freeze' => 'Your account has been frozen',
    'block' => 'Your account has been hacked',
    'error' => 'Account status error',
    'freeze_time_error' => 'Freeze time cannot be less than the current time',
  ),
  'exists' => 'User already exists',
  'no_exists' => 'user does not exist',
  'expire' => 'Login timeout, please log in again',
  'socialite' => 
  array (
    'user_error' => 'The resolved user and the transmitted user are inconsistent',
  ),
  'account' => 
  array (
    'type_error' => 'Account type error',
  ),
  'authenticator_secret' => 
  array (
    'not_bind' => 'No binding for secondary validation',
    'secret_exists_not_code' => 'You have bound for secondary verification, please verify the original key',
    'verify_error' => 'Validation error',
    'verify_error_old' => 'Old key, verification error',
    'verify_error_new' => 'New key, verification error',
    'verify_error_password' => 'Password verification error',
  ),
  'captcha' => 
  array (
    'img' => 'Image verification code error',
    'email' => 'Email verification code error',
    'mobile' => 'SMS verification code error',
  ),
  'bind' => 
  array (
    'exists' => 
    array (
      'mobile' => 'The phone number is already bound and cannot be repeatedly bound',
      'email' => 'The email is already bound and cannot be repeatedly bound',
    ),
  ),
  'init' => 
  array (
    'exists' => 
    array (
      'safe_password' => 'The security password has already been initialized and cannot be initialized again',
    ),
  ),
  'no_bind' => 
  array (
    'mobile' => 'Unbound phone number',
    'email' => 'Unbound mailbox',
  ),
  'authentication' => 
  array (
    'accomplish' => 
    array (
      'bind_real_name_error' => 'Authenticated with real name and cannot change name',
    ),
    'certified' => 
    array (
      'bind_real_name_error' => 'Authenticated with real name and cannot change name',
    ),
    'exists' => 'Certified, please do not repeat the certification',
    'not_token' => 'Please brush your face for authentication',
    'not_pay' => 'Real name authentication requires payment, please make the payment first',
    'not_log_info' => 'No authentication information found',
    'error' => 'Certification failed',
    'success' => 'Certification successful',
    'success_hint_pay' => 'Successfully identified, please make payment in a timely manner',
    'not_need_pay' => 'Certification does not require payment. If you encounter difficulties, please contact the administrator',
    'after_need_authentication' => 'Please authenticate before making payment',
    'verify_before_payment' => 'Please verify before making payment',
    'id_number_pass' => 'The ID number number of this authentication has been authenticated',
    'id_number_length_error' => 'The length of ID number is illegal',
    'id_number_check_code_error' => 'The inspection code of ID number is illegal',
    'id_number_check_code_bit_error' => 'ID number inspection code error',
    'id_number_birthday_error' => 'ID number Date of birth is illegal',
    'id_number_territory_error' => 'ID number is illegal',
    'artificial_auth_wait' => 'Successfully submitted, please wait for the review result',
  ),
);