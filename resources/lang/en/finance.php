<?php return array (
  'error' => 
  array (
    'unknown' => 'No such order',
    'pay_driver' => 'Wrong order channel',
    'amount' => 'The order value is incorrect',
  ),
  'recharge' => 
  array (
    'error' => 
    array (
      'min' => 'The recharge amount cannot be less than: min',
      'max' => 'The recharge amount cannot be greater than: max',
      'multiple' => 'The recharge amount must be an integer multiple of :multiple',
      'repetition' => 'Orders cannot be reused',
    ),
    'success' => 'Recharged successfully',
  ),
  'transfer' => 
  array (
    'success' => 'Transfer successful',
    'error' => 'Transfer failed',
    'receipt' => 
    array (
      'success' => 'Accepted successfully',
      'error' => 'Accept failed',
    ),
    'refuse' => 
    array (
      'success' => 'Rejected successfully',
      'error' => 'Reject failed',
    ),
  ),
  'convert' => 
  array (
    'success' => 'Redemption successful',
    'error' => 'Redemption failed',
    'wallet_eq' => 'Converting wallets cannot choose the same option',
    'switch' => ':fromWallet cannot be exchanged with :wallet',
  ),
  'withdraw' => 
  array (
    'channel_error' => 'This driver method is not currently available for withdrawal',
    'auto_error' => 'Automatic withdrawal failed',
  ),
  'wallet' => 
  array (
    'unknown' => 'There is currently no such wallet available',
    'minus' => ':walletName is insufficient',
    'not_exists' => 'User wallet does not exist',
    '余额' => 'balance',
    '大币' => 'Large coin',
    '小币' => 'Small coin',
    '积分' => 'integral',
    'not_open' => 'This wallet has not been opened yet',
  ),
  'pay_way_error' => 'Incorrect payment method',
  '账户微信' => 'Account WeChat',
  'award' => 
  array (
    'register_close' => 'Registration reward has been closed',
  ),
  'pay' => 
  array (
    'driver_close' => 'The payment method has been closed. Please try a different one and try it out',
    'wallet_close' => 'This wallet payment method has been closed. Please try a different one and try it out',
    'success' => 'Payment successful',
    'error' => 'Payment failed',
  ),
  'red_package' => 
  array (
    'receive' => 'The red envelope has been collected completely',
    'expire' => 'The red envelope has expired',
    'error' => 'Failed to receive red envelope',
    'draw_exists' => 'Red envelopes cannot be claimed repeatedly',
    'receiver_error' => 'This red envelope was not sent to you, you cannot claim it',
    'receiver_empty_error' => 'Please specify the recipient',
    'friend_group_empty_error' => 'Please specify the group to send to',
    'status_error' => 'Red envelope status error',
    'draw_friend' => 'Only friends can receive this red envelope',
    'friend_group_error' => 'Friend group error',
    'friend_group_send_error' => 'You are not in this group and cannot send red envelopes',
    'close' => 'The red envelope sending function has been turned off',
    'wallet_close' => 'The red envelope function of this wallet has been turned off',
    'min_error' => 'The red envelope sent is too small and must be greater than: min',
    'max_error' => 'The red envelope sent is too large and must be less than: max',
    'max_num_error' => 'Too many red packets sent, must be less than: max',
    'money_too_small' => 'The total amount of the red envelope you sent is too small and not enough for one person to handle',
  ),
);