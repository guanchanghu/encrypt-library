<?php return array (
  'file_exist' => 'This file has already been uploaded!',
  'file_not_exist' => 'The uploaded file does not exist!',
  'file_extension_error' => 'The upload file format is incorrect, not in :extension format!',
  'copy_suffix_null' => 'The suffix \':suffix\' is currently not supported',
  'original' => 
  array (
    'not_exist' => 'Please pass on the original file information',
    'not_name' => 'Please pass the original file name',
    'not_size' => 'Please pass the original file size',
    'not_type' => 'Please pass the original file type',
    'name_error' => 'The original file name does not match the one you passed in',
    'size_error' => 'The original file name size does not match the one you passed in',
    'type_error' => 'The original file name type does not match the one you passed in',
  ),
  'forbid' => 'Prohibit uploading files in this :extension format',
);