<?php

return [
    // 使用什么翻译驱动
    // 目前支持: "baidu", "youdao", "google"
    /**
     *  默认使用google  google使用的是免费接口爬取，目前能用，为了确保稳定，请配置一个备用服务， 目前只有google和baidu 支持繁体翻译
     **/

    'defaults' => [
        'driver' => env('TRANSLATE_DEFAULT_DRIVER', 'baidu'),   //默认使用google翻译
        'spare_driver' => env('TRANSLATE_DEFAULT_SPARE_DRIVER', 'baidu'),  // 备用翻译api ,第一个翻译失败情况下，调用备用翻译服务，填写备用翻译api 需要在下面对应的drivers中配置你参数
        'from' => env('TRANSLATE_DEFAULT_FROM', 'en'),   //原文本语言类型 ，目前支持：auto【自动检测】,en【英语】,zh【中文】，jp【日语】,ko【韩语】，fr【法语】，ru【俄文】，pt【西班牙】
        'to' => env('TRANSLATE_DEFAULT_TO', 'zh'),     //翻译文本 ：en【英语】,zh【中文】，jp【日语】,ko【韩语】，fr【法语】，ru【俄文】，pt【西班牙】,
    ],

    'drivers' => [
        'baidu' => [
            'base_url' => env('TRANSLATE_BAIDU_BASE_URL', 'https://api.fanyi.baidu.com/api/trans/vip/translate'),
            'app_id' => env('TRANSLATE_BAIDU_APP_ID', ''),
            'app_key' => env('TRANSLATE_BAIDU_APP_KEY', ''),
            'interval' => env('TRANSLATE_BAIDU_INTERVAL', 0), //每次翻译的间隔 单位秒
        ],

        'youdao' => [
            'base_url' => env('TRANSLATE_YOU_DAO_BASE_URL', 'https://openapi.youdao.com/api'),
            'app_id' => env('TRANSLATE_YOU_DAO_APP_ID', ''),
            'app_key' => env('TRANSLATE_YOU_DAO_APP_KEY', ''),
            'interval' => env('TRANSLATE_YOU_DAO_INTERVAL', 0), //每次翻译的间隔 单位秒
        ],

        'google' => [
            'base_url' => env('TRANSLATE_GOOGLE_BASE_URL', 'http://translate.google.cn/translate_a/single'),
            'app_id' => env('TRANSLATE_GOOGLE_APP_ID', ''),
            'app_key' => env('TRANSLATE_GOOGLE_APP_KEY', ''),
            'interval' => env('TRANSLATE_GOOGLE_INTERVAL', ''), //每次翻译的间隔 单位秒
        ],
    ],
];
