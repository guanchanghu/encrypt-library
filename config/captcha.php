<?php

return [
    'characters' => ['2', '3', '4', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'm', 'n', 'p', 'q', 'r', 't', 'u', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'M', 'N', 'P', 'Q', 'R', 'T', 'U', 'X', 'Y', 'Z'],

    /**
     * 默认way
     */
    'way_default' => 'character',

    /**
     * way方式
     */
    'way' => [
        'character' => [
            'key' => 'character',
        ],
        'chinese' => [
            'key' => 'chinese',
        ],
        'math' => [
            'key' => 'math',
        ],
    ],

    'default' => [
        'length' => 4,
        'width' => 160,
        'height' => 46,
        'quality' => 90,
        'maxBehindLine' => 2,
        'sensitive' => true,
        'lineColor' => [100, 124, 210],
        'backgroundColor' => [220, 210, 230],
        'font' => null,
        'expire' => 120,
        'distortion' => false,
        'interpolate' => false,
        'math' => [
            'result' => [
                'min' => 1,
                'max' => 100,
            ],
            'digit' => 2,
            'operator' => [
                'add' => 25,
                'sub' => 25,
                'mul' => 25,
                'div' => 25,
            ],
            'decimal' => 0,
        ],
    ],

    'custom' => [
        'characters' => ['2', '3', '4', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'm', 'n', 'p', 'q', 'r', 't', 'u', 'x', 'y', 'z', 'A', 'B', 'D', 'E', 'F', 'G', 'H', 'J', 'M', 'N', 'P', 'Q', 'R', 'T', 'U', 'X', 'Y', 'Z'],
        'length' => 4,
        'width' => 160,
        'height' => 46,
        'quality' => 90,
        'maxBehindLine' => 2,
        'sensitive' => true,
        'lineColor' => [100, 124, 210],
        'backgroundColor' => [220, 210, 230],
        'font' => null,
        'expire' => 120,
        'distortion' => false,
        'interpolate' => false,
        'math' => [
            'result' => [
                'min' => 1,
                'max' => 100,
            ],
            'digit' => 2,
            'operator' => [
                'add' => 25,
                'sub' => 25,
                'mul' => 25,
                'div' => 25,
            ],
            'decimal' => 0,
        ],
    ],
];
