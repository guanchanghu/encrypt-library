<?php


return [
    'captcha_length' => 6,
    'captcha_lifetime' => 120,
    'captcha_code_key' => 'code',
    'hz_limit' => [
        'captcha' => 120 / 60,
        'notification' => 0,
    ],

    'drivers' => [
        // 短信内容使用 template + data
        'ali' => [
            'driver' => 'ali',
            'access_key_id' => '',
            'access_key_secret' => '',
            'sign_name' => '',
        ],
        /**
         * 腾讯云 SMS
         * 短信内容使用 template + data
         */
        'tencent' => [
            'driver' => 'tencent',
            'secret_id' => '', // SECRET ID
            'secret_key' => '', // SECRET KEY
            'sdk_app_id' => '', // 短信应用的 SDK APP ID
            'sign_name' => 'xx公司', // 短信签名
        ],
        // 短信内容使用 template + data
        'baidu' => [
            'driver' => 'baidu',
            'ak' => '',
            'sk' => '',
            'invoke_id' => '',// 签名ID
        ],
        // 短信内容使用 template + data
        'qiniu' => [
            'driver' => 'qiniu',
            'secret_key' => '',
            'access_key' => '',
        ],
        // 短信内容使用 template + data
        'juhe' => [
            'driver' => 'juhe',
            'app_key' => '',
        ],
        // 短信分为两大类，验证类和通知类短信。 发送验证类短信使用 template + data
        'rongCloud' => [
            'driver' => 'rongCloud',
            'app_key' => '',
            'app_secret' => '',
        ],
        // 短信内容使用 template + data
        'huawei' => [
            'driver' => 'huawei',
//                'endpoint' => '', // APP接入地址, 有默认的
            'app_key' => '', // APP KEY
            'app_secret' => '', // APP SECRET
            'from' => [
                'default' => '', // 默认使用签名通道号
//                    'custom' => 'csms12345', // 其他签名通道号 可以在 data 中定义 from 来指定
//                    'abc' => 'csms67890', // 其他签名通道号
            ],
            'callback' => '' // 短信状态回调地址
        ],
        // 网易云信
        // 短信内容使用 template + data
        'yunxin' => [
            'driver' => 'yunxin',
            'app_key' => '',
            'app_secret' => '',
            'code_length' => 6, // 随机验证码长度，范围 4～10，默认为 4
            'need_up' => false, // 是否需要支持短信上行
        ],
        // 短信宝
        // 短信使用 content
        'smsBao' => [
            'driver' => 'smsBao',
            // 这里模板ID 配置成 您的验证码为: ${code}
            'user' => '',    //账号
            'password' => ''   //密码
        ],
    ],
];
