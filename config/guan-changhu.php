<?php

return [
    /**
     * 用户root用户
     */
    'root_account' => 'admin',

    /**
     * models配置文件
     */
    'models' => [
        'user' => \GuanChanghu\Library\Models\User::class,
        'user_client' => \GuanChanghu\Library\Models\UserClient::class,
        'user_account' => \GuanChanghu\Library\Models\UserAccount::class,
        'auth_custom' => \GuanChanghu\Library\Models\AuthCustom::class,
        'email_account' => \GuanChanghu\Library\Models\EmailAccount::class,
        'email_send_log' => \GuanChanghu\Library\Models\EmailSendLog::class,
        'email_send_statistics_log' => \GuanChanghu\Library\Models\EmailSendStatisticsLog::class,
        'file_store_record' => \GuanChanghu\Library\Models\FileStoreRecord::class,
        'message_content' => \GuanChanghu\Library\Models\MessageContent::class,
        'pay_log' => \GuanChanghu\Library\Models\PayLog::class,
        'rsa_secret_key' => \GuanChanghu\Library\Models\RsaSecretKey::class,
        'sms_send_log' => \GuanChanghu\Library\Models\SmsSendLog::class,
        'territory' => \GuanChanghu\Library\Models\Territory::class,
        'thrift_auth' => \GuanChanghu\Library\Models\ThriftAuth::class,
        'user_message' => \GuanChanghu\Library\Models\UserMessage::class,
        'user_socialite' => \GuanChanghu\Library\Models\UserSocialite::class,
        'user_wallet' => \GuanChanghu\Library\Models\UserWallet::class,
        'user_wallet_log' => \GuanChanghu\Library\Models\UserWalletLog::class,
        'authentication_log' => \GuanChanghu\Library\Models\AuthenticationLog::class,
        'statistics_record' => \GuanChanghu\Library\Models\StatisticsRecord::class,

        'question' => '',
        'question_option' => '',
        'question_subject' => '',
        'question_difficulty' => '',
        'question_tag' => '',
    ],


    /**
     * 地域导入文件
     */
    'territory_lead_file' => env('TERRITORY_DEFAULT_FILE', 'territory.json'),

    /**
     * client 配置文件
     */
    'clients' => [
        /**
         * 客户端模块命名空间
         */
        'module_namespace' => '\\App\\Http\\Clients\\',

        /**
         * 模型请求 UUID
         */
        'module_request_uuid' => 'RE',

        /**
         * 客户端list
         */
        'list' => [
            'admin',
            'web',
            'h5',
            'wap',
            'agent',
        ],
        /**
         * and 标识
         */
        'and' => [
            'admin' => 1,
            'web' => 2,
            'h5' => 4,
            'wap' => 8,
            'agent' => 16,
        ],

        /**
         * 默认客户端
         */
        'default_client' => 'web',

        /**
         * 默认客户端
         */
        'default_clients' => [
            'web',
            'h5',
            'wap',
        ],

        /**
         * 模块开关json文件
         */
        'module_switch_json_file' => 'module.switch.json',

        /**
         * 权限客户端开关
         */
        'permission_client_switch' => [
            'admin' => false,
            'web' => false,
            'h5' => false,
            'wap' => false,
            'agent' => false,
        ],

        /**
         * 输入日志类型 0 - 关闭  1 - 同步  2 - swoole异步  3 - 同步队列  4 - 异步队列
         */
        'input_log_type' => env('INPUT_LOG_TYPE', 0),
        /**
         * 输出日志类型 0 - 关闭  1 - 同步  2 - swoole异步  3 - 同步队列  4 - 异步队列
         */
        'output_log_type' => env('OUTPUT_LOG_TYPE', 0),
        /**
         * 是否独自记录运行时间日志 0 - 关闭  1 - 同步  2 - swoole异步  3 - 同步队列  4 - 异步队列
         */
        'alone_output_run_time_log_type' => env('ALONE_OUTPUT_RUN_TIME_LOG_TYPE', 0),
        /**
         * 慢日志类型 0 - 关闭  1 - 同步  2 - swoole异步  3 - 同步队列  4 - 异步队列
         */
        'slow_log_time_type' => env('SLOW_LOG_TIME_TYPE', 0),
        /**
         * 慢日志的时间界限,单位毫秒,如果是100,说明运行时间超过100毫秒的记录下来
         */
        'slow_log_time_limit' => env('SLOW_LOG_TIME_LIMIT', 0),
        /**
         * 监听db类型 0 - 关闭  1 - 同步  2 - swoole异步  3 - 同步队列  4 - 异步队列
         */
        'listen_db' => env('LISTEN_DB', 0),
    ],

    'auth' => [
        'default' => 'custom',
        /**
         * 系统方式是否询问数据库
         */
        'custom_inquiry_database' => false,
        /**
         * 自定义表id前缀
         */
        'custom_id_prefix' => 'AC',
    ],

    'log' => [
        /**
         * 日志类型-默认
         */
        'type_default' => 1,

        /**
         * 日志类型-task
         */
        'type_task' => 2,

        /**
         * 日志类型-queue 同步
         */
        'type_queue_sync' => 3,

        /**
         * 日志类型-queue 异步
         */
        'type_queue_async' => 3,
    ],

    'encrypt' => [
        'clients_encrypt_enable' => [
            'admin' => false,
            'web' => false,
            'h5' => false,
            'wap' => false,
            'agent' => false,
        ],

        /**
         * bits
         */
        'bits' => 2048,

        /**
         * 运行rsa的数量
         */
        'run_client_number' => 20,

        /**
         * 客户端rsa
         */
        'rsa_key_list' => [
            'decrypt' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器解密用,客户端使用公钥加密,服务器用私钥解密,客户端保留公钥',
            ],
            'encrypt_admin' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器返回请求加密用,服务器使用公钥加密,客户端使用私钥解密,客户端保留私钥',
            ],
            'encrypt_web' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器返回请求加密用,服务器使用公钥加密,客户端使用私钥解密,客户端保留私钥',
            ],
            'encrypt_agent' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器返回请求加密用,服务器使用公钥加密,客户端使用私钥解密,客户端保留私钥',
            ],
            'encrypt_h5' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器返回请求加密用,服务器使用公钥加密,客户端使用私钥解密,客户端保留私钥',
            ],
            'encrypt_wap' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器返回请求加密用,服务器使用公钥加密,客户端使用私钥解密,客户端保留私钥',
            ],
            'sign' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器生成签名用,服务器使用私钥生成签名,客户端使用公钥验证签名,客户端保留公钥',
            ],
            'verify_admin' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器验证签名使用,客户端使用私钥生成签名,服务器使用公钥验证签名,客户端保留私钥',
            ],
            'verify_agent' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器验证签名使用,客户端使用私钥生成签名,服务器使用公钥验证签名,客户端保留私钥',
            ],
            'verify_h5' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器验证签名使用,客户端使用私钥生成签名,服务器使用公钥验证签名,客户端保留私钥',
            ],
            'verify_wap' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器验证签名使用,客户端使用私钥生成签名,服务器使用公钥验证签名,客户端保留私钥',
            ],
            'verify_web' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '服务器验证签名使用,客户端使用私钥生成签名,服务器使用公钥验证签名,客户端保留私钥',
            ],
            'auth_custom' => [
                'encryptionMode' => OPENSSL_PKCS1_PADDING,
                'signatureMode' => OPENSSL_ALGO_SHA256,
                'comment' => '自定义授权使用',
            ],
        ],
    ],

    'question' => [
        'option' => [
            'no' => 0,
            'yes' => 1,
            'undefined' => 2,
        ],
        'type' => [
            'enum' => [
                'radio' => 1,
                'checkbox' => 2,
                'indefinite_checkbox' => 3,
                'judge' => 4,
                'close' => 5,
                'part' => 6,
                'theory' => 7,
            ],
            'translate' => [
                'radio' => '单项选择题',
                'checkbox' => '不定项选择',
                'indefinite_checkbox' => '多项选择题',
                'judge' => '判断题',
                'close' => '完形填空',
                'part' => '阅读理解',
                'theory' => '论答题',
            ],
            'label' => [
                1 => '单项选择题',
                2 => '不定项选择',
                3 => '多项选择题',
                4 => '判断题',
                5 => '完形填空',
                6 => '阅读理解',
                7 => '论答题',
            ],
        ],
    ],

    'pay_driver' => [
        'ali' => '支付宝',
        'weChat' => '微信',
        'wallet' => '钱包支付',
    ],

    'certificate_enum' => [
        0 => '身份证',
        1 => '驾驶证',
        2 => '护照',
    ],

    'authentication' => [
        'default' => 'ali',
        'drivers' => [
            'ali' => [
                'driver' => 'ali',
                'label' => '阿里',
                'isToken' => true,
                'accessKeyId' => '',
                'accessKeySecret' => '',
                'with' => [
                    'SceneId' => '', // 认证场景ID
                    'CertType' => 'IDENTITY_CARD', // 	您的终端用户传入的证件类型。 当前仅支持身份证，取值固定为IDENTITY_CARD。
                    'ProductCode' => 'ID_PRO', // 要接入的认证方案
                    'Model' => 'LIVENESS', // 增强版实人认证的业务场景类型  要进行活体检测的类型。取值：
                    'CallbackUrl' => '',
                ],
            ],
            'juhe' => [
                'driver' => 'juhe',
                'label' => '聚合',
                'isToken' => false,
                'gateway' => 'http://op.juhe.cn/idcard/query',
                'key' => '',
            ],
            'tencent' => [
                'driver' => 'tencent',
                'label' => '腾讯',
            ],
            'artificial' => [
                'driver' => 'tencent',
                'label' => '人工',
            ],
        ],
        'auth_status' => [
            // 未认证
            'unverified' => 0,
            // 认证前(已付款)
            'before_paid' => 1,
            // 已认证(未付款)
            'certified_not_paid' => 2,
            // 认证完成
            'accomplish' => 3,
        ],
        'order_id_prefix' => 'UA',
    ],

    'throttle' => [
        /**
         * 没有登录用户的redis key
         */
        'not_login_user_redis_key' => 'throttle:not:login:user',

        /**
         * 登录用户的redis key
         */
        'login_user_redis_key' => 'throttle:login:user',

        /**
         * 多少个用户分配一个key
         */
        'user_number_unit' => 50,

        /**
         * redis生存是时间,单位秒,必须大于 REQUEST_LIFETIME_RANGE 两个的和否则会有问题
         */
        'redis_lift_time' => 210,
    ],

    'email' => [
        'queue' => 'email',
        'captcha_length' => 6,
        'captcha_lifetime' => 120,
    ],
    'pay' => [
        'default_driver' => 'ali',
        'drivers' => [
            /**
             * 支付驱动-支付宝
             */
            'ali' => [
                'driver' => 'ali',
            ],

            /**
             * 支付驱动-微信
             */
            'weChat' => [
                'driver' => 'alweChati',
            ],
        ],
        'ways' => [
            /**
             * 支付方式-web
             */
            'web' => [
                'way' => 'web',
            ],

            /**
             * 支付方式-wap
             */
            'wap' => [
                'way' => 'wap',
            ],

            /**
             * 支付方式-app
             */
            'app' => [
                'way' => 'app',
            ],

            /**
             * 支付方式-pos
             */
            'pos' => [
                'way' => 'pos',
            ],

            /**
             * 支付方式-mini
             */
            'mini' => [
                'way' => 'mini',
            ],

            /**
             * 支付方式-scan
             */
            'scan' => [
                'way' => 'scan',
            ],

            /**
             * 支付方式-mp
             */
            'mp' => [
                'way' => 'mp',
            ],
        ],
    ],
    'repository' => [
        'default_way' => 'precise',
        'ways' => [
            /**
             * 分页方式-粗糙的
             * total 使用max
             */
            'rough' => [
                'way' => 'rough',
            ],

            /**
             * 分页方式-精确的
             * total 使用count
             */
            'precise' => [
                'way' => 'precise',
            ],

            /**
             * 分页方式-快速的
             * total 不统计
             */
            'quick' => [
                'way' => 'quick',
            ],
        ],
    ],

    'upload' => [
        'default_driver' => 'local',

        'drivers' => [
            'local' => [
                'driver' => 'local',
            ],
        ],

        /**
         * 禁止上传的枚举
         */
        'forbid_enum' => [
            'php',
            'java',
            'go',
            'py',
            'js',
            'ts',
            'exe',
        ],

        /**
         * 默认存储文件夹
         */
        'default_storage_directory' => 'file',

        /**
         * 图片存储文件夹
         */
        'image_storage_directory' => 'images',

        /**
         * 视频存储文件夹
         */
        'video_storage_directory' => 'video',

        /**
         * app存储文件夹
         */
        'app_storage_directory' => 'app',

        /**
         * document存储文件夹
         */
        'document_storage_directory' => 'document',

        /**
         * package存储文件夹
         */
        'package_storage_directory' => 'package',

        /**
         * file存储文件夹
         */
        'file_storage_directory' => 'file',

        /**
         * 图片存储文件夹后缀
         */
        'image_storage_directory_extension_enum' => ['img', 'jpg', 'jpeg', 'png',],

        /**
         * 视频存储文件夹后缀
         */
        'video_storage_directory_extension_enum' => [
            'mp4', 'avi', 'wmv', 'mpg', 'mov', 'rm', 'ram', 'swf', 'flv',
        ],

        /**
         * 视频存储文件夹后缀
         */
        'app_storage_directory_extension_enum' => ['apk', 'ipa', 'exe', 'dmg',],

        /**
         * 文档存储文件夹后缀
         */
        'document_storage_directory_extension_enum' => [
            'doc', 'docx', 'pdf', 'xls', 'xlsx', 'pptx', 'ppt',
        ],

        /**
         * package存储文件夹后缀
         */
        'package_storage_directory_extension_enum' => [
            'zip', 'tar', 'rar', 'z', 'lzh', 'jar'
        ],
    ],

    'verification' => [
        'default_driver' => 'ali',
        'drivers' => [
            'ali' => [
                'driver' => 'ali',

                /**
                 * 阿里 access key id
                 */
                'access_key_id' => '',

                /**
                 * 阿里 secret
                 */
                'secret' => '',

                /**
                 * 阿里 配置
                 */
                'app_key' => '',

                /**
                 * 阿里-智能验证方式
                 */
                'analyze_nvc' => 'analyzeNvc',
            ],
            'google' => [
                'driver' => 'google',

                /**
                 * 谷歌 配置
                 */
                'secret' => '',
            ],
        ],
    ],

    'web_socket' => [
        /**
         * web socket hash key
         */
        'hash_key_prefix' => 'web:socket:room:key:',

        /**
         * web socket channel key
         */
        'channel_key_prefix' => 'web-socket-channel:',
    ],

    'finance' => [
        'symbol_income' => 0,
        'symbol_expend' => 1,
    ],
];